import { Injectable } from '@angular/core';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PlansService {

  selectedPlan = new Subject<any>();


  updateSelectedPlan(plan: string) {
    this.selectedPlan.next(plan);
  }

  getSelectedPlan(): Observable<any> {
    return this.selectedPlan.asObservable();
  }


  constructor() { }
}

export const PLANS: Plan[] = [
  {
    'name': 'SiriusXM Select',
    'key': 'SiriusXMSelect',
    'price': 15.99,
    'subscription': 'https://care.siriusxm.com/login_view.action?pkg=SE',
  },
  {
    'name': 'Mostly Music',
    'key': 'SiriusXMMostlyMusic',
    'price': 10.99,
    'subscription': 'https://care.siriusxm.com/login_view.action?pkg=MM',
  },
  {
    'name': 'All Access',
    'key': 'siriusxmallaccess',
    'price': 19.99,
    'subscription': 'https://care.siriusxm.com/login_view.action?pkg=AA',
  }
];

export interface Plan {
  name: string;
  key: string;
  price: number;
  subscription: string;
}
