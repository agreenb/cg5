import { Injectable } from '@angular/core';
import {Observable, Subject, Subscription} from 'rxjs';
import {Genre} from '../cg/models/genre';
import {CategoriesService} from './categories.service';

@Injectable({
  providedIn: 'root'
})
export class GenresService {
  private genres = new Subject<Genre[]>();
  private selectedGenre = new Subject<any>();

  selectedCategory: string;
  subscription: Subscription;

  constructor(private categoriesService: CategoriesService) {
    this.subscription = this.categoriesService.getCategory()
      .subscribe(category => {
        this.selectedCategory = category;
        // console.log('genre service category' + category);
      });

  }

  updateGenre(name: string, index: number) {
    this.selectedGenre.next({name: name, index: index});
  }

  getGenre(): Observable<any> {
    return this.selectedGenre.asObservable();
  }

  updateGenres() {
    this.genres.next(GENRES.filter(genre => genre.category === this.selectedCategory));
  }

  getGenres(): Observable<Genre[]> {
    return this.genres.asObservable();
  }
}

export const GENRES: Genre[] = [
  {name: 'All Music', key: 'All Music', category: 'Music', },
  {name: 'Rock', key: 'Rock', category: 'Music', },
  {name: 'Pop', key: 'Pop', category: 'Music', },
  {name: 'Country', key: 'Country', category: 'Music', },
  {name: 'Hip-Hop/R&B', key: 'RandB_Hip-Hop', category: 'Music', },
  {name: 'Christian', key: 'Christian', category: 'Music', },
  {name: 'Jazz/Standards', key: 'Jazz/Standards', category: 'Music', },
  {name: 'Classical', key: 'Classical', category: 'Music', },
  {name: 'Dance/Electronic', key: 'Dance/Electronic', category: 'Music', },
  {name: 'Latino', key: 'Latino', category: 'Music', },
  {name: 'Holiday', key: 'Holiday', category: 'Music', },
  {name: 'Canadian', key: 'Canadian', category: 'Music', },

  {name: 'Sports', key: 'Sports', category: 'Sports', },
  {name: 'NFL Play-by-Play', key: 'NFL Play-by-Play', category: 'Sports', },
  {name: 'MLB Play-by-Play', key: 'MLB Play-by-Play', category: 'Sports', },
  {name: 'College Play-by-Play', key: 'College Play-by-Play', category: 'Sports', },
  {name: 'NBA Play-by-Play', key: 'NBA Play-by-Play', category: 'Sports', },
  {name: 'NHL Play-by-Play', key: 'NHL Play-by-Play', category: 'Sports', },
  {name: 'Sports Play-by-Play', key: 'Sports Play-by-Play', category: 'Sports', },

  {name: 'All Talk', key: 'talk', category: 'Talk & Entertainment', },
  {name: 'Entertainment', key: 'Entertainment', category: 'Talk & Entertainment', },
  {name: 'Comedy', key: 'Comedy', category: 'Talk & Entertainment', },
  {name: 'Religion', key: 'Religion', category: 'Talk & Entertainment', },
  {name: 'Kids', key: 'Kids', category: 'Talk & Entertainment', },
  {name: 'More', key: 'More', category: 'Talk & Entertainment', },

  {name: 'All News', key: 'all_news', category: 'News & Issues', },
  {name: 'News/Public Radio', key: 'news_public_radio', category: 'News & Issues', },
  {name: 'Politics/Issues', key: 'Politics/Issues', category: 'News & Issues', },
  {name: 'Traffic/Weather', key: 'Traffic/Weather', category: 'News & Issues', },

  {name: 'Howard Stern', key: 'howard', category: 'Howard Stern', },

  {name: 'All', key: 'all', category: 'All'},
];
