import { Injectable } from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {Category} from '../cg/models/category';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  private selectedCategory = new Subject<any>();

  updateCategory(category: string) {
    this.selectedCategory.next(category);
  }

  getCategory(): Observable<any> {
    return this.selectedCategory.asObservable();
  }

}

export const CATEGORIES: Category[] =  [
  {
    name : 'Music',
    key: 'music',
  }, {
    name : 'Sports',
    key: 'sports',
  }, {
    name : 'Talk & Entertainment',
    key: 'talk',
  }, {
    name: 'News & Issues',
    key: 'news',
  }, {
    name: 'Howard Stern',
    key: 'howard',
  }, {
    name : 'All',
    key: 'all',
  }
];
