import {Component, OnInit, QueryList} from '@angular/core';
import {Plan, PLANS, PlansService} from '../../shared/plans.service';
import {Subscription} from 'rxjs';
import {ObservableMedia} from '@angular/flex-layout';
import {FormBuilder, FormGroup} from '@angular/forms';
import {CategoriesService} from '../../shared/categories.service';
import {GenresService} from '../../shared/genres.service';
import {Genre} from '../../cg/models/genre';
import {MatOption} from '@angular/material';

@Component({
  selector: 'cg-btn-row',
  templateUrl: './btn-row.component.html',
  styleUrls: ['./btn-row.component.scss']
})
export class BtnRowComponent {

  plans: Plan[] = PLANS;
  planSelected: string;
  category: string;
  genre: string;
  selectedPlanSub: Subscription;
  selectedGenreSub: Subscription;
  selectedCategorySub: Subscription;
  options: QueryList<MatOption>;

  constructor(private plansService: PlansService, private categoriesService: CategoriesService, private genresService: GenresService ) {
    this.selectedPlanSub = this.plansService.getSelectedPlan().subscribe(plan => this.planSelected = plan);
    this.selectedCategorySub = this.categoriesService.getCategory().subscribe( category => this.category = category);
    this.selectedGenreSub = this.genresService.getGenre().subscribe(genre => this.genre = genre.name);

  }

  updateSelectedPlan(plan: string) {
    this.plansService.updateSelectedPlan(plan);
  }


}
