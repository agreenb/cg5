import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import {AppMaterialModule} from '../z_app-material/app-material.module';
import {CgModule} from '../cg/cg.module';
import { BtnRowComponent } from './components/btn-row.component';

@NgModule({
  declarations: [
    AppComponent,
    BtnRowComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    AppMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    RouterModule,
    CgModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
