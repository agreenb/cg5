import { Injectable } from '@angular/core';
import {GenresService} from '../../shared/genres.service';
import {CategoriesService} from '../../shared/categories.service';
import {Observable, Subject, Subscription} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PromoService {
  public promos = new Subject<Promo[]>();
  genreSub: Subscription;
  selectedGenre: string;
  categorySub: Subscription;
  selectedCategory: string;

  constructor(private genresService: GenresService,  private categoriesService: CategoriesService) {
    this.genreSub = this.genresService.getGenre()
      .subscribe(genre => {
        this.selectedGenre = genre.name;
        console.log('genre' + genre.name);
      });
    this.categorySub = categoriesService.getCategory()
      .subscribe(category => {
        this.selectedCategory = category;
      });
  }

  updatePromos(): void {
    this.promos.next(PROMOS.filter(promo => promo.genre === this.selectedGenre));
    console.log('updatted promos with ' + this.selectedGenre);
    // this.selectedGenre.startsWith('All') ?
    //   this.promos.next(PROMOS.filter(promo => promo.category === this.selectedCategory)) :
    //   this.promos.next(PROMOS.filter(promo => promo.genre === this.selectedGenre));
  }

  getPromos(): Observable<Promo[]>{
    // console.log()
    return this.promos.asObservable();
  }
}


export const PROMOS: Promo[] = [
  {
    header: 'one_header',
    text: 'Queen',
    key: 'one_key',
    category: 'Music',
    genre: 'Rock',
    position: 3,
    imgUrl: 'https://www.billboard.com/files/styles/article_main_image/public/media/queen-portrait-circa-1975-billboard-1548.jpg',
  }, {
    header: 'two_header',
    text: 'Ariana Grande',
    key: 'two_key',
    category: 'Music',
    genre: 'Pop',
    position: 6,
    imgUrl: 'https://kaigai-drama-board.com/assets/medias/2017/05/201705-N0091840-main.jpg',
  },{
    header: 'sport_header',
    text: 'Sports sports sports',
    key: 'two_key',
    category: 'Sports',
    genre: 'Sports',
    position: 2,
    imgUrl: 'https://cdn.vox-cdn.com/thumbor/NwaDaHcae4J4dLYFWYGDEsLwfYQ=/0x0:787x390/1200x800/filters:focal(332x133:456x257)/cdn.vox-cdn.com/uploads/chorus_image/image/60361731/Screen_Shot_2018_07_13_at_12.07.28_PM.0.png',
  }, {
    header: 'two_header',
    key: 'music_key',
    text: 'All Music Feature',
    category: 'Music',
    genre: 'All Music',
    position: 2,
  }
];

export class Promo {
  header: string;
  text: string;
  key: string;
  category: string;
  genre: string;
  position?: number;
  imgUrl?: string;
}
