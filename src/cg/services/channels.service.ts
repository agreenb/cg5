import { Injectable } from '@angular/core';
import {Observable, Subject, Subscription} from 'rxjs';
import {Channel} from '../models/channel';
import {CHANNELS} from '../models/channels';
import {GenresService} from '../../shared/genres.service';
import {CategoriesService} from '../../shared/categories.service';

@Injectable({
  providedIn: 'root'
})
export class ChannelsService {
  public channels = new Subject<Channel[]>();
  private selectedGenre: string;
  genreSub: Subscription;
  categorySub: Subscription;
  selectedCategory: string;

  constructor(private genresService: GenresService, private categoriesService: CategoriesService) {
    this.genreSub = this.genresService.getGenre()
      .subscribe(genre => {
        this.selectedGenre = genre.name;
      });
    this.categorySub = categoriesService.getCategory()
      .subscribe(category => {
        this.selectedCategory = category;
      });

  }

  updateChannels(): void {
    this.selectedGenre.startsWith('All') ?
      this.channels.next(CHANNELS.filter(channel => channel.progtypetitle === this.selectedCategory)) :
      this.channels.next(CHANNELS.filter(channel => channel.genretitle === this.selectedGenre));

  }
  getChannels(): Observable<Channel[]> {
    return this.channels.asObservable();
  }

}
