import {AfterViewInit, Component, ElementRef, Input, OnDestroy, OnInit, Renderer2} from '@angular/core';
import {Genre} from '../../models/genre';
import {Subscription} from 'rxjs';
import {GenresService} from '../../services';
import {ChannelsService} from '../../services/channels.service';
import {MatTabBody, MatTabChangeEvent} from '@angular/material';
import {Renderer3} from '@angular/core/src/render3/interfaces/renderer';
import {PromoService} from '../../services/promo.service';

@Component({
  selector: 'cg-genres',
  templateUrl: './genres.component.html',
  styleUrls: ['./genres.component.scss']
})
export class GenresComponent implements OnInit, OnDestroy, AfterViewInit {

  genres: Genre[];
  genresSubscription: Subscription;
  selectedIndex = 0;
  selectedGenreSub: Subscription;

  constructor(private genresService: GenresService,
              private channelsService: ChannelsService,
              private promoService: PromoService) {
    this.genresSubscription = this.genresService.getGenres()
      .subscribe(genres => {
        this.genres = genres;
      });


    this.selectedGenreSub = this.genresService.getGenre()
      .subscribe(
        genre => this.selectedIndex = genre.index);

  }

  ngAfterViewInit() {
  }


  updateGenre(event: MatTabChangeEvent){
    this.genresService.updateGenre(event.tab.ariaLabel, this.selectedIndex);
    this.channelsService.updateChannels();
    this.promoService.updatePromos();
  }


  ngOnInit() {
    this.genresService.updateGenres();
  }

  ngOnDestroy() {

  }

}
