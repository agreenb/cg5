import {AfterContentInit, Component, OnInit, TemplateRef, ViewChild, ViewContainerRef} from '@angular/core';
import {Plan, PLANS, PlansService} from '../../../shared/plans.service';
import {Subscription} from 'rxjs';
import { MediaChange, ObservableMedia } from '@angular/flex-layout';
import {animate, animateChild, query, stagger, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'cg-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  animations: [
    trigger('items', [
      transition(':enter', [
        style({ transform: 'scale(0.5)', opacity: 0 }),  // initial
        animate('0.7s cubic-bezier(.8, -0.6, 0.26, 1.6)',
          style({ transform: 'scale(1)', opacity: 1 }))  // final
      ]),
      transition(':leave', [
        style({ transform: 'scale(1)', opacity: 1, height: '*' }),
        animate('0.6s cubic-bezier(.8, -0.6, 0.2, 1.5)',
          style({
            transform: 'scale(0.5)', opacity: 0,
            height: '0px', margin: '0px'
          }))
      ])
    ]),
    trigger('list', [
      transition(':enter', [
        query('@items', stagger(150, animateChild()))
      ]),
    ])
  ],
})
export class MenuComponent implements OnInit, AfterContentInit {
  title = 'CHANNEL GUIDE';
  plans: Plan[] = PLANS;
  planSelected: string;
  selectedPlanSub: Subscription;
  onMobile : boolean;
  winWidth: number;
  screenSize: string;

  constructor(private plansService: PlansService, private observableMedia: ObservableMedia) {
    this.selectedPlanSub = this.plansService.getSelectedPlan().subscribe(plan => this.planSelected = plan);
  }

  updateSelectedPlan(plan: string) {
    this.plansService.updateSelectedPlan(plan);
  }

  updateResponsive(size?: string) {
    this.screenSize = size;
    this.winWidth = window.innerWidth;
    this.onMobile = this.winWidth <= 750;
    console.log('menu onMobile: ' + this.onMobile);
  }


  ngAfterContentInit() {

    this.observableMedia.asObservable().subscribe((change: MediaChange) => {
      // console.log(change);
      console.log(window.innerWidth);
      this.updateResponsive(change.mqAlias);
    });
}


  ngOnInit() {
    this.updateSelectedPlan('siriusxmallaccess');
    this.updateResponsive();
  }
}

