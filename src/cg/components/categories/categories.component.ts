import {Component, OnInit} from '@angular/core';
import {CATEGORIES, CategoriesService, GenresService} from '../../services';
import {Category} from '../../models/category';
import {Genre} from '../../models/genre';
import {Subscription} from 'rxjs';
import {ChannelsService} from '../../services/channels.service';
import {PromoService} from '../../services/promo.service';
import {MatTabChangeEvent} from '@angular/material';
import {animate, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'cg-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss'],
  animations: [
    trigger('search', [
      transition(':enter', [
        style({ transform: 'translateX(-100%)', opacity: 1,  }),  // initial
        // animate('100'),
        // animate('0.7s',
        animate('0.4s cubic-bezier(.8, 0.6, 0.26, 1.2)',
          style({ transform: 'translateX(0%)', opacity: 1, }))  // final
      ]),
      transition(':leave', [
        style({ transform: 'scale(1)', opacity: 1, height: '*' }),
        animate('0.6s cubic-bezier(.8, -0.6, 0.2, 1.5)',
          style({
            transform: 'scale(0.5)', opacity: 0,
            height: '0px', margin: '0px'
          }))
      ])
    ])
  ]
})
export class CategoriesComponent implements OnInit {

  categories: Category[] = CATEGORIES;
  genres: Genre[];
  genresSubscription: Subscription;
  genreSub: Subscription;
  selectedGenre: Genre;

  selectedIndex = 0;
  searchOpen = false;

  // ===============================================
  // ===               Constructor               ===
  // ===============================================

  constructor(private genresService: GenresService,
              private categoriesService: CategoriesService,
              private channelsService: ChannelsService,
              private promoService: PromoService ) {
    this.genresSubscription = this.genresService.getGenres()
      .subscribe(genres => {
        this.genres = genres;
      });
    this.genreSub = this.genresService.getGenre().subscribe(genre => this.selectedGenre = genre);
  }
  getCategory(category: MatTabChangeEvent){
    console.log('category change: ' + category.tab);

  }

  updateCategory(category: string) {
    this.categoriesService.updateCategory(category);
    this.genresService.updateGenres();
    this.genresService.updateGenre(this.genres[0].name, 0);
    // this.selectedGenre = this.genres[0];
    this.channelsService.updateChannels();
    this.promoService.updatePromos();
  }



  ngOnInit() {
    this.updateCategory(this.categories[0].name);
  }

}


