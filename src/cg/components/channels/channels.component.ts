import {AfterContentInit, Component, OnInit, ViewChild} from '@angular/core';
import {Channel} from '../../models/channel';
import {Subscription} from 'rxjs';
import {ChannelsService} from '../../services/channels.service';
import {PlansService} from '../../../shared/plans.service';
import {MediaChange, ObservableMedia} from '@angular/flex-layout';
import {GenresService} from '../../services';
import {Promo, PromoService} from '../../services/promo.service';
import {MatGridList} from '@angular/material';


// export interface Tile {
//   color: string;
//   cols: number;
//   rows: number;
//   text: string;
// }


@Component({
  selector: 'cg-channels',
  templateUrl: './channels.component.html',
  styleUrls: ['./channels.component.scss']
})
export class ChannelsComponent implements OnInit, AfterContentInit {
  @ViewChild('grid') grid: MatGridList;
  // @ViewChild('tile') leftTile: MatGridTile;
  screenSize: string;
  rowGutter: number;
  channels: Channel[];
  channelsSub: Subscription;
  promoSub: Subscription;
  planSelected: string;
  planSelectedSub: Subscription;
  winWidth: number;
  onMobile: boolean;
  promos: Promo[];

  gridByBreakpoint = {
    // xl: {
    //   bottomSpace: .1,
    //   cols: 10,
    //   rowHeight: '35px'
    // },
    // lg: {
    //   bottomSpace: .1,
    //   cols: 10,
    //   rowHeight: '35px'
    // },
    // md: {
    //   bottomSpace: .1,
    //   cols: 10,
    //   rowHeight: '35px'
    // },
    // sm: {
    //   bottomSpace: .2,
    //   cols: 16,
    //   rowHeight: '38px'
    // },
    // xs: {
    //   bottomSpace: .2,
    //   cols: 4,
    //   rowHeight: '75px'
    // },
    xl: {
      bottomSpace: .1,
      cols: 10,
      rowHeight: '35px'
    },
    lg: {
      bottomSpace: .1,
      cols: 10,
      rowHeight: '35px'
    },
    md: {
      bottomSpace: .1,
      cols: 10,
      rowHeight: '35px'
    },
    sm: {
      bottomSpace: .2,
      cols: 10,
      rowHeight: '38px'
    },
    xs: {
      bottomSpace: .2,
      cols: 10,
      rowHeight: '75px'
    },
  };

  // tileList: any[] = [
  //   {
  //     name: 'xs',
  //     tiles1: [
  //       {text: 'One', cols: 3, rows: 1, color: 'lightblue'},
  //       {text: 'Two', cols: 1, rows: 2, color: 'lightgreen'},
  //       {text: 'Three', cols: 1, rows: 1, color: 'lightpink'},
  //       {text: 'Three', cols: 1, rows: 1, color: 'lightpink'},
  //       {text: 'Three', cols: 1, rows: 1, color: 'lightpink'},
  //       {text: 'Four', cols: 2, rows: 1, color: '#DDBDF1'}
  //     ]
  //   },{
  //     name: 'sm',
  //     tiles1: [
  //       {text: 'One', cols: 3, rows: 1, color: 'lightblue'},
  //       {text: 'Two', cols: 1, rows: 2, color: 'lightgreen'},
  //       {text: 'Three', cols: 1, rows: 1, color: 'lightpink'},
  //       {text: 'Three', cols: 1, rows: 1, color: 'lightpink'},
  //       {text: 'Three', cols: 1, rows: 1, color: 'lightpink'},
  //       {text: 'Four', cols: 2, rows: 1, color: '#DDBDF1'}
  //     ]
  //   },{
  //     name: 'med',
  //     tiles1: [
  //       {text: 'One', cols: 3, rows: 1, color: 'lightblue'},
  //       {text: 'Two', cols: 1, rows: 2, color: 'lightgreen'},
  //       {text: 'Three', cols: 1, rows: 1, color: 'lightpink'},
  //       {text: 'Three', cols: 1, rows: 1, color: 'lightpink'},
  //       {text: 'Four', cols: 2, rows: 1, color: '#DDBDF1'}
  //     ]
  //   },{
  //     name: 'lg',
  //     tiles1: [
  //       {text: 'One', cols: 3, rows: 1, color: 'lightblue'},
  //       {text: 'Two', cols: 1, rows: 2, color: 'lightgreen'},
  //       {text: 'Three', cols: 1, rows: 1, color: 'lightpink'},
  //       {text: 'Three', cols: 1, rows: 1, color: 'lightpink'},
  //       {text: 'Three', cols: 1, rows: 1, color: 'lightpink'},
  //       {text: 'Four', cols: 2, rows: 1, color: '#DDBDF1'}
  //     ]
  //   },{
  //     name: 'xl',
  //     tiles1: [
  //       {text: 'One', cols: 3, rows: 1, color: 'lightblue'},
  //       {text: 'Two', cols: 1, rows: 2, color: 'lightgreen'},
  //       {text: 'Three', cols: 1, rows: 1, color: 'lightpink'},
  //       {text: 'Three', cols: 1, rows: 1, color: 'lightpink'},
  //       {text: 'Three', cols: 1, rows: 1, color: 'lightpink'},
  //       {text: 'Three', cols: 1, rows: 1, color: 'lightpink'},
  //       {text: 'Three', cols: 1, rows: 1, color: 'lightpink'},
  //       {text: 'Four', cols: 2, rows: 1, color: '#DDBDF1'}
  //     ]
  //   }
  // ];

// {
//   name: 'sm',
//   tiles: [{text: 'One', cols: 3, rows: 1, color: 'lightblue'},
//     {text: 'Two', cols: 1, rows: 2, color: 'lightgreen'},
//     {text: 'Three', cols: 1, rows: 1, color: 'lightpink'},
//     {text: 'Three', cols: 1, rows: 1, color: 'lightpink'},
//     {text: 'Three', cols: 1, rows: 1, color: 'lightpink'},
//     {text: 'Four', cols: 2, rows: 1, color: '#DDBDF1'},]
// }

  constructor(private channelsService: ChannelsService,
              private plansService: PlansService,
              private observableMedia: ObservableMedia,
              private genresService: GenresService,
              private promoService: PromoService) {
    this.channelsSub = this.channelsService.getChannels()
      .subscribe(channels => {this.channels = channels.slice(0, 20); });
    this.planSelectedSub = this.plansService.getSelectedPlan()
      .subscribe(plan => {
        this.planSelected = plan;
      });
    this.promoSub = this.promoService.getPromos().subscribe(promos => this.promos = promos);
  }

  updateResponsive() {
    this.winWidth = window.innerWidth;
    this.onMobile = this.winWidth <= 750;
    console.log('channels onMobile: ' + this.onMobile);
  }

  ngAfterContentInit() {
    this.observableMedia.asObservable().subscribe((change: MediaChange) => {
      this.grid.cols = this.gridByBreakpoint[change.mqAlias].cols;
      this.grid.rowHeight = this.gridByBreakpoint[change.mqAlias].rowHeight;
      this.screenSize = change.mqAlias;
      this.rowGutter = this.gridByBreakpoint[change.mqAlias].bottomSpace;
      console.log(this.screenSize);
      this.updateResponsive();
    });
  }

  ngOnInit() {
    this.updateResponsive();
  }


}

