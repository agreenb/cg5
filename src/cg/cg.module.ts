import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule, NoopAnimationsModule} from '@angular/platform-browser/animations';

import { FormsModule } from '@angular/forms';
import {AppMaterialModule} from '../z_app-material/app-material.module';
import { MenuComponent } from './components/menu/menu.component';
import { GenresComponent } from './components/genres/genres.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { ChannelsComponent } from './components/channels/channels.component';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [
    MenuComponent,
    GenresComponent,
    CategoriesComponent,
    ChannelsComponent,

  ],
  imports: [
    CommonModule,
    FormsModule,
    AppMaterialModule,
    // NoopAnimationsModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
  ],
  exports: [
    MenuComponent,
    GenresComponent,
    CategoriesComponent,
    ChannelsComponent,

  ],
  providers: [],
  bootstrap: []
})
export class CgModule { }
