import {Channel} from './channel';

export const CHANNELS: Channel[] = [
  {
    id: '9412',
    displayname: 'Celebrate!',
    shortdescription: 'Happy Songs For A Celebration',
    progtypetitle: 'Music',
    genretitle: 'Pop',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/Celebrate',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293952995305&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 300,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 300,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'leftofcenter',
    displayname: 'SiriusXMU',
    shortdescription: 'New Indie Rock',
    progtypetitle: 'Music',
    genretitle: 'Rock',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxmu',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795937&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 35,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 35,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9370',
    displayname: 'FOX News Talk',
    shortdescription: 'Talk Radio from FOX News',
    progtypetitle: 'News & Issues',
    genretitle: 'Politics/Issues',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/foxnewstalk',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795164&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 450,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 450,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'rawdog',
    displayname: 'Raw Dog Comedy Hits',
    shortdescription: 'The Best Uncensored Comedy XL',
    progtypetitle: 'Talk & Entertainment',
    genretitle: 'Comedy',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/rawdogcomedyhits',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1439176824528&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 99,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 99,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9285',
    displayname: 'New York Knicks',
    shortdescription: 'Knicks Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NBA Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nbanyk',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1388171542102&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 899,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 899,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9456',
    displayname: 'SiriusXM 372',
    shortdescription: 'SiriusXM 372',
    progtypetitle: 'Sports',
    genretitle: 'Sports',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxm372',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293953002847&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 372,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 372,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8295',
    displayname: 'NHL Play-by-Play 223',
    shortdescription: 'Live NHL® Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NHL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nhlplaybyplay223',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283872600875&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 223,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 223,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9261',
    displayname: 'Sports Play-by-Play 980',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay980',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 980,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 980,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'bstnpa',
    displayname: 'Boston/Philadelphia/Washington DC',
    shortdescription: 'Boston/Philadelphia/Wash',
    progtypetitle: 'News & Issues',
    genretitle: 'Traffic/Weather',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/bostonphiladelphiawashingtondc',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283876544320&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 134,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 134,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '8294',
    displayname: 'NHL Play-by-Play 222',
    shortdescription: 'Live NHL® Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NHL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nhlplaybyplay222',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283872600875&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 222,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 222,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '8266',
    displayname: 'MLB en Español',
    shortdescription: 'MLB News & Games',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/mlbenespanol',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283891940775&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 159,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 159,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '8372',
    displayname: 'Neil Diamond Radio',
    shortdescription: 'Neil Diamond, 24/7',
    progtypetitle: 'Music',
    genretitle: 'Pop',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/neildiamondradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283875054174&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 700,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 700,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'thespectrum',
    displayname: 'The Spectrum',
    shortdescription: 'Classic Rock Meets New Rock',
    progtypetitle: 'Music',
    genretitle: 'Rock',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/thespectrum',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796128&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 28,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 28,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'cnn',
    displayname: 'CNN',
    shortdescription: 'CNN Simulcast',
    progtypetitle: 'News & Issues',
    genretitle: 'News/Public Radio',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/cnn',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009734093&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 116,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 116,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'siriushits1',
    displayname: 'SiriusXM Hits 1',
    shortdescription: 'Today\'s Pop Hits',
    progtypetitle: 'Music',
    genretitle: 'Pop',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxmhits1',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795843&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 2,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 2,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'espnradio',
    displayname: 'ESPN Radio',
    shortdescription: 'Golic & Wingo / Sports Talk',
    progtypetitle: 'Sports',
    genretitle: 'Sports',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/espnradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283872600418&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 80,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 80,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9234',
    displayname: 'Boston Red Sox',
    shortdescription: 'Red Sox Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/bostonredsoxonline',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283888353155&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 843,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 843,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8229',
    displayname: 'enLighten',
    shortdescription: 'Southern Gospel',
    progtypetitle: 'Music',
    genretitle: 'Christian',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/enlighten',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009734658&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 65,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 65,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9420',
    displayname: 'Yacht Rock Radio',
    shortdescription: '\'70s/\'80s Smooth-Sailing Soft Rock',
    progtypetitle: 'Music',
    genretitle: 'Rock',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/yachtrockradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293947943941&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 311,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 311,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9300',
    displayname: 'Columbus Blue Jackets',
    shortdescription: 'Blue Jackets Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NHL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/columbusbluejackets',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283900801293&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 928,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 928,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9406',
    displayname: 'Pitbull\'s Globalization',
    shortdescription: 'Worldwide Rhythmic Hits',
    progtypetitle: 'Music',
    genretitle: 'Pop',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/pitbullsglobalization',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293943626462&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 13,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 13,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9220',
    displayname: 'Sports Play-by-Play 960',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay960',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 960,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 960,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'buzzsaw',
    displayname: 'Ozzy\'s Boneyard',
    shortdescription: 'Ozzy\'s Classic Hard Rock',
    progtypetitle: 'Music',
    genretitle: 'Rock',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/ozzysboneyard',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283884703290&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 38,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 38,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9436',
    displayname: 'Sports Play-by-Play 386',
    shortdescription: 'Sports play by play',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay386',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293948976534&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 386,
        packages: {
          siriusxmallaccess: true
        }
      },
      online: {
        number: 386,
        packages: {
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9258',
    displayname: 'Texas Rangers',
    shortdescription: 'Rangers Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/texasrangersonline',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283888407959&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 867,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 867,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9318',
    displayname: 'Pittsburgh Penguins',
    shortdescription: 'Penguins Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NHL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/pittsburghpenguins',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283900650713&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 942,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 942,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9133',
    displayname: 'American Latino Radio',
    shortdescription: 'American Latino Talk Radio',
    progtypetitle: 'More',
    genretitle: 'More',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/americanlatinoradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1422733940205&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 154,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 154,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9302',
    displayname: 'Chicago Blackhawks',
    shortdescription: 'Blackhawks Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NHL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/chicagoblackhawks',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283900805070&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 926,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 926,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8293',
    displayname: 'NHL Play-by-Play 221',
    shortdescription: 'Live NHL® Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NHL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nhlplaybyplay221',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283872600875&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 221,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 221,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9283',
    displayname: 'Minnesota Timberwolves',
    shortdescription: 'Timberwolves Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NBA Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nbamin',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1387826454426&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 897,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 897,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9266',
    displayname: 'Atlanta Hawks',
    shortdescription: 'Hawks Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NBA Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nbaatl',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293938942939&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 880,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 880,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'bloombergradio',
    displayname: 'Bloomberg Radio',
    shortdescription: 'Business News',
    progtypetitle: 'News & Issues',
    genretitle: 'News/Public Radio',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/bloombergradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283876497720&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 119,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 119,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9158',
    displayname: 'Houston Texans',
    shortdescription: 'Texans Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/houstontexans',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283892892571&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 812,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 812,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9228',
    displayname: 'Sports Play-by-Play 968',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay968',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 968,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 968,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9414',
    displayname: 'EL PAISA',
    shortdescription: 'Your Latino Variety Channel',
    progtypetitle: 'More',
    genretitle: 'More',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/elpaisa',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293944186668&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 470,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 470,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9223',
    displayname: 'Sports Play-by-Play 963',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay963',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 963,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 963,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9203',
    displayname: 'Los Angeles Rams',
    shortdescription: 'Rams Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/losangelesrams',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283892648675&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 817,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 817,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'hiphopnation',
    displayname: 'Hip-Hop Nation',
    shortdescription: 'Today\'s Hip-Hop Hits-XL',
    progtypetitle: 'Music',
    genretitle: 'Hip-Hop/R&B',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/hiphopnation',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795291&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 44,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 44,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9146',
    displayname: 'Arizona Cardinals',
    shortdescription: 'Cardinals Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/arizonacardinals',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283892883069&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 800,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 800,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9210',
    displayname: 'Sports Play-by-Play 973',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay973',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 973,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 973,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9297',
    displayname: 'Boston Bruins',
    shortdescription: 'Bruins Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NHL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/bostonbruins',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283900632180&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 922,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 922,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'starlite',
    displayname: 'The Blend',
    shortdescription: 'Bright Pop Hits',
    progtypetitle: 'Music',
    genretitle: 'Pop',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/theblend',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796044&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 16,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 16,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9326',
    displayname: 'Sports Play-by-Play 982',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay982',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 982,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 982,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'radiomargaritaville',
    displayname: 'Radio Margaritaville',
    shortdescription: 'Escape to Margaritaville',
    progtypetitle: 'Music',
    genretitle: 'Rock',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/radiomargaritaville',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795697&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 24,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 24,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8183',
    displayname: 'SiriusXM Insight',
    shortdescription: 'Podcasts & Political Comedians',
    progtypetitle: 'News & Issues',
    genretitle: 'News/Public Radio',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxminsight',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1416010500255&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 121,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 121,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9253',
    displayname: 'San Diego Padres',
    shortdescription: 'Padres Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sandiegopadresonline',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283888374040&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 862,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 862,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9427',
    displayname: 'Sports Play-by-Play 958',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay958',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 958,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 958,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9296',
    displayname: 'Anaheim Ducks',
    shortdescription: 'Ducks Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NHL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/anaheimducks',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283900629972&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 920,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 920,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8239',
    displayname: 'PRX Public Radio',
    shortdescription: 'Independent Public Radio',
    progtypetitle: 'News & Issues',
    genretitle: 'News/Public Radio',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/publicradioexchange',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795667&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 123,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 123,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9392',
    displayname: 'Joel Osteen Radio',
    shortdescription: 'Positive Inspiration For Life',
    progtypetitle: 'Talk & Entertainment',
    genretitle: 'Entertainment',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/joelosteenradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1407527417498&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 128,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 128,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9418',
    displayname: 'No Shoes Radio',
    shortdescription: 'Kenny Chesney\'s Music Channel',
    progtypetitle: 'Music',
    genretitle: 'Country',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/noshoesradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1496906651421&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 57,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 57,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9249',
    displayname: 'New York Yankees',
    shortdescription: 'Yankees Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/newyorkyankeesonline',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283888359964&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 858,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 858,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9329',
    displayname: 'Sports Play-by-Play 985',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay985',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 985,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 985,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8332',
    displayname: 'Pac-12 Play-by-Play 198',
    shortdescription: 'Live Pac-12 Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'College Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/pac12playbyplay198',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1397562783691&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 198,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 198,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9390',
    displayname: 'TODAY Show Radio',
    shortdescription: 'The TODAY Show - All Day',
    progtypetitle: 'Talk & Entertainment',
    genretitle: 'Entertainment',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/todayshowradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1397569177888&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 108,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 108,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9224',
    displayname: 'Sports Play-by-Play 964',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay964',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 964,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 964,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9298',
    displayname: 'Buffalo Sabres',
    shortdescription: 'Sabres Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NHL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/buffalosabres',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283900633704&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 923,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 923,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9248',
    displayname: 'New York Mets',
    shortdescription: 'Mets Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/newyorkmetsonline',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283888359926&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 857,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 857,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'rumbon',
    displayname: 'Caliente',
    shortdescription: 'Tropical Latin Music',
    progtypetitle: 'Music',
    genretitle: 'Pop',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/caliente',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009733738&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 158,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 158,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9299',
    displayname: 'Carolina Hurricanes',
    shortdescription: 'Hurricanes Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NHL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/carolinahurricanes',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283900796771&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 925,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 925,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'nprnow',
    displayname: 'NPR Now',
    shortdescription: 'NPR News & Conversation',
    progtypetitle: 'News & Issues',
    genretitle: 'News/Public Radio',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nprnow',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795511&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 122,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 122,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'chill',
    displayname: 'SiriusXM Chill',
    shortdescription: 'Downtempo/Deep House',
    progtypetitle: 'Music',
    genretitle: 'Dance/Electronic',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/chill',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795831&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 53,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 53,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8292',
    displayname: 'NHL Play-by-Play 220',
    shortdescription: 'Live NHL® Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NHL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nhlplaybyplay220',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283872600875&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 220,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 220,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9267',
    displayname: 'Brooklyn Nets',
    shortdescription: 'Nets Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NBA Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nbabkn',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293938957620&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 882,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 882,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8368',
    displayname: 'SiriusXM Fantasy Sports Radio',
    shortdescription: 'Fantasy Sports Talk',
    progtypetitle: 'Sports',
    genretitle: 'Sports',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxmfantasysportsradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009835722&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 87,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 87,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'radioclassics',
    displayname: 'RadioClassics',
    shortdescription: 'Classic Radio Shows',
    progtypetitle: 'Talk & Entertainment',
    genretitle: 'Entertainment',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/radioclassics',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795685&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 148,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 148,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9437',
    displayname: 'Sports Play-by-Play 387',
    shortdescription: 'Sports play by play',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay387',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293948976588&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 387,
        packages: {
          siriusxmallaccess: true
        }
      },
      online: {
        number: 387,
        packages: {
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9174',
    displayname: 'Rock and Roll Hall of Fame Radio',
    shortdescription: 'Rock Hall Inducted Artists',
    progtypetitle: 'Music',
    genretitle: 'Rock',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/rockandrollhalloffameradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1512382770562&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 310,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 310,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '10000',
    displayname: 'SiriusXM Preview',
    shortdescription: 'SiriusXM Preview',
    progtypetitle: 'Talk & Entertainment',
    genretitle: 'Entertainment',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxmpreview',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1443393010673&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 794,
        packages: {
          SiriusXMSelect: false,
          SiriusXMMostlyMusic: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 794,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8325',
    displayname: 'NFL Play-by-Play 233',
    shortdescription: 'Home for every NFL game',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nflplaybyplay233',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293940121536&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 233,
        packages: {
          siriusxmallaccess: true
        }
      },
      online: {
        number: 233,
        packages: {
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '8367',
    displayname: 'MSNBC',
    shortdescription: 'MSNBC Simulcast',
    progtypetitle: 'News & Issues',
    genretitle: 'News/Public Radio',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/msnbc',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795469&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 118,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 118,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9438',
    displayname: 'Sports Play-by-Play 388',
    shortdescription: 'Sports play by play',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay388',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293948976689&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 388,
        packages: {
          siriusxmallaccess: true
        }
      },
      online: {
        number: 388,
        packages: {
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9325',
    displayname: 'Winnipeg Jets',
    shortdescription: 'Jets Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NHL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/winnipegjets',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283900676094&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 950,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 950,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8228',
    displayname: 'The Groove',
    shortdescription: '\'70s/\'80s R&B',
    progtypetitle: 'Music',
    genretitle: 'Hip-Hop/R&B',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/thegroove',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796080&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 50,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 50,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9307',
    displayname: 'Florida Panthers',
    shortdescription: 'Panthers Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NHL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/floridapanthers',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1512381186336&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 932,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 932,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8291',
    displayname: 'NHL Play-by-Play 219',
    shortdescription: 'Live NHL® Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NHL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nhlplaybyplay219',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283872600875&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 219,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 219,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9426',
    displayname: 'Sports Play-by-Play 957',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay957',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 957,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 957,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9472',
    displayname: 'Diplo\'s Revolution',
    shortdescription: 'Top Rhythm Music',
    progtypetitle: 'Music',
    genretitle: 'Dance/Electronic',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/diplosrevolution',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293957660782&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 52,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 52,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8320',
    displayname: 'NFL Play-by-Play 228',
    shortdescription: 'Home for every NFL game',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nflplaybyplay228',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293940121536&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 228,
        packages: {
          siriusxmallaccess: true
        }
      },
      online: {
        number: 228,
        packages: {
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9139',
    displayname: 'SiriusXM Limited Edition 2',
    shortdescription: 'Home for limited-run channels',
    progtypetitle: 'Music',
    genretitle: 'Rock',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxmlimitededition2',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293944373401&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 716,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 716,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9162',
    displayname: 'Miami Dolphins',
    shortdescription: 'Dolphins Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/miamidolphins',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283892893666&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 818,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 818,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9150',
    displayname: 'Carolina Panthers',
    shortdescription: 'Panthers Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/carolinapanthers',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283892887548&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 804,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 804,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8307',
    displayname: 'Family Talk',
    shortdescription: 'Christian Talk',
    progtypetitle: 'Talk & Entertainment',
    genretitle: 'Religion',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/familytalk',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283877398694&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 131,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 131,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8314',
    displayname: 'Sports Play-by-Play 207',
    shortdescription: 'Sports Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay207',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 207,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 207,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '8213',
    displayname: 'Mad Dog Sports Radio',
    shortdescription: 'Mad Dog Russo, Morning Men',
    progtypetitle: 'Sports',
    genretitle: 'Sports',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/maddogsportsradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795397&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 82,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 82,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9352',
    displayname: 'Tom Petty\'s Buried Treasure',
    shortdescription: 'Tom Petty’s Buried Treasure 24/7',
    progtypetitle: 'Music',
    genretitle: 'Rock',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/tompettysburiedtreasure',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283928884257&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 711,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 711,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9274',
    displayname: 'Detroit Pistons',
    shortdescription: 'Pistons Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NBA Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nbadet',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293939060629&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 888,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 888,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9310',
    displayname: 'Montreal Canadiens',
    shortdescription: 'Canadiens Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NHL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/montrealcanadiens',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283900809815&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 935,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 935,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8301',
    displayname: 'Sports Play-by-Play 202',
    shortdescription: 'Sports Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay202',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 202,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 202,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: 'symphonyhall',
    displayname: 'Symphony Hall',
    shortdescription: 'Classical Music',
    progtypetitle: 'Music',
    genretitle: 'Classical',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/symphonyhall',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796032&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 76,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 76,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9424',
    displayname: 'Sports Play-by-Play 955',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay955',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 955,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 955,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9205',
    displayname: 'Tennessee Titans',
    shortdescription: 'Titans Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/tennesseetitans',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283892632972&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 830,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 830,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'roaddogtrucking',
    displayname: 'Road Dog Trucking',
    shortdescription: 'Talk for Truckers',
    progtypetitle: 'Talk & Entertainment',
    genretitle: 'Entertainment',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/roaddogtrucking',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795755&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 146,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 146,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9455',
    displayname: 'SiriusXM ACC Radio',
    shortdescription: '24/7 ACC Talk & Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'Sports',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxmaccradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293953002702&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 371,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 371,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9231',
    displayname: 'Arizona Diamondbacks',
    shortdescription: 'Diamondbacks Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/arizonadiamondbacksonline',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283888302171&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 840,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 840,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9246',
    displayname: 'Milwaukee Brewers',
    shortdescription: 'Brewers Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/milwaukeebrewersonline',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283888359440&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 855,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 855,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9147',
    displayname: 'Atlanta Falcons',
    shortdescription: 'Falcons Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/atlantafalcons',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283892885799&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 801,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 801,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8269',
    displayname: 'MLB Play-by-Play 178',
    shortdescription: 'Live MLB® Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/mlbplaybyplay178',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795439&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 178,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 178,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: 'radiodisney',
    displayname: 'Radio Disney',
    shortdescription: 'Pop Hits For The Entire Family',
    progtypetitle: 'Talk & Entertainment',
    genretitle: 'Kids',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/radiodisney',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283877397042&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 79,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 79,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9331',
    displayname: 'Sports Play-by-Play 987',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay987',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 987,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 987,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8184',
    displayname: 'Faction Talk',
    shortdescription: 'Norton/Roberts/Ellis/Covino&Rich-XL',
    progtypetitle: 'Talk & Entertainment',
    genretitle: 'Entertainment',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/factiontalk',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1512380482880&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 103,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 103,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9276',
    displayname: 'Houston Rockets',
    shortdescription: 'Rockets Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NBA Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nbahou',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293938969437&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 890,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 890,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9439',
    displayname: 'Sports Play-by-Play 389',
    shortdescription: 'Sports play by play',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay389',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293948976761&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 389,
        packages: {
          siriusxmallaccess: true
        }
      },
      online: {
        number: 389,
        packages: {
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '8241',
    displayname: 'KIIS-Los Angeles',
    shortdescription: 'KIIS FM LOS Angeles',
    progtypetitle: 'More',
    genretitle: 'More',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/kiis',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283904217601&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 11,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 11,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9291',
    displayname: 'San Antonio Spurs',
    shortdescription: 'Spurs Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NBA Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nbasa',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293939017511&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 906,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 906,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9222',
    displayname: 'Sports Play-by-Play 962',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay962',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 962,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 962,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8319',
    displayname: 'NFL Play-by-Play 227',
    shortdescription: 'Home for every NFL game',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nflplaybyplay227',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293940121536&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 227,
        packages: {
          siriusxmallaccess: true
        }
      },
      online: {
        number: 227,
        packages: {
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9320',
    displayname: 'St. Louis Blues',
    shortdescription: 'Blues Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NHL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/stlouisblues',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283900663470&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 944,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 944,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'bluegrass',
    displayname: 'Bluegrass Junction',
    shortdescription: 'Bluegrass',
    progtypetitle: 'Music',
    genretitle: 'Country',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/bluegrassjunction',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009732910&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 62,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 62,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9156',
    displayname: 'Detroit Lions',
    shortdescription: 'Lions Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/detroitlions',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283892891661&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 810,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 810,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9161',
    displayname: 'Kansas City Chiefs',
    shortdescription: 'Chiefs Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/kansascitychiefs',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283892893451&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 815,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 815,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9337',
    displayname: 'Sports Play-by-Play 993',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay993',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 993,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 993,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8225',
    displayname: 'Viva',
    shortdescription: 'Today\'s Latin Pop Hits',
    progtypetitle: 'Music',
    genretitle: 'Latino',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/viva',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796186&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 763,
        packages: {
          SiriusXMSelect: false,
          SiriusXMMostlyMusic: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 763,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9425',
    displayname: 'Sports Play-by-Play 956',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay956',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 956,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 956,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'icebergradio',
    displayname: 'Iceberg',
    shortdescription: 'The New Rock Alternative',
    progtypetitle: 'More',
    genretitle: 'Canadian',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/icebergradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283932527959&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 758,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 758,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9148',
    displayname: 'Baltimore Ravens',
    shortdescription: 'Ravens Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/baltimoreravens',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283892886277&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 802,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 802,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9179',
    displayname: 'Krishna Das Yoga Radio',
    shortdescription: 'Chant/Sacred/Spiritual Music',
    progtypetitle: 'Music',
    genretitle: 'Jazz/Standards',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/krishnadasyoga',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283881513723&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 751,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 751,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'altnation',
    displayname: 'Alt Nation',
    shortdescription: 'New Alternative Rock',
    progtypetitle: 'Music',
    genretitle: 'Rock',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/altnation',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293947938288&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 36,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 36,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'reggaerhythms',
    displayname: 'The Joint',
    shortdescription: 'Reggae',
    progtypetitle: 'Music',
    genretitle: 'Hip-Hop/R&B',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/thejoint',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293946289960&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 42,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 42,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9272',
    displayname: 'Dallas Mavericks',
    shortdescription: 'Mavericks Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NBA Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nbadal',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293938968799&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 886,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 886,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9356',
    displayname: 'Comedy Central Radio',
    shortdescription: 'Comedy Central Uncensored XL',
    progtypetitle: 'Talk & Entertainment',
    genretitle: 'Comedy',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/comedycentralradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1443393010673&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 95,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 95,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9262',
    displayname: 'Sports Play-by-Play 981',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay981',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 981,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 981,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9216',
    displayname: 'Sports Play-by-Play 979',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay979',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 979,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 979,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8324',
    displayname: 'NFL Play-by-Play 232',
    shortdescription: 'Home for every NFL game',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nflplaybyplay232',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293940121536&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 232,
        packages: {
          siriusxmallaccess: true
        }
      },
      online: {
        number: 232,
        packages: {
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9389',
    displayname: 'Venus',
    shortdescription: 'Pop Music You Can Move To',
    progtypetitle: 'Music',
    genretitle: 'Pop',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/venus',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293944195508&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 3,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 3,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'ny',
    displayname: 'New York',
    shortdescription: 'New York',
    progtypetitle: 'News & Issues',
    genretitle: 'Traffic/Weather',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/newyork',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283876544320&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 133,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 133,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9423',
    displayname: 'Sports Play-by-Play 954',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay954',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 954,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 954,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9313',
    displayname: 'New York Islanders',
    shortdescription: 'Islanders Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NHL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/newyorkislanders',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283900813124&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 938,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 938,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9256',
    displayname: 'St. Louis Cardinals',
    shortdescription: 'Cardinals Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/stlouiscardinalsonline',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283888375611&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 865,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 865,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8370',
    displayname: 'Pearl Jam Radio',
    shortdescription: 'Pearl Jam 24/7',
    progtypetitle: 'Music',
    genretitle: 'Rock',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/pearljamradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283872768262&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 22,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 22,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9332',
    displayname: 'Sports Play-by-Play 988',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay988',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 988,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 988,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9440',
    displayname: 'Sports Play-by-Play 390',
    shortdescription: 'Sports play by play',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay390',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293948976853&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 390,
        packages: {
          siriusxmallaccess: true
        }
      },
      online: {
        number: 390,
        packages: {
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: 'classicvinyl',
    displayname: 'Classic Vinyl',
    shortdescription: '\'60s/\'70s Classic Rock',
    progtypetitle: 'Music',
    genretitle: 'Rock',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/classicvinyl',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009734061&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 26,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 26,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'spa73',
    displayname: 'Spa',
    shortdescription: 'New Age',
    progtypetitle: 'Music',
    genretitle: 'Jazz/Standards',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/spa',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795985&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 68,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 68,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8227',
    displayname: 'The Village',
    shortdescription: 'Folk',
    progtypetitle: 'Music',
    genretitle: 'Country',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/thevillage',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796180&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 741,
        packages: {
          SiriusXMSelect: false,
          SiriusXMMostlyMusic: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 741,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9178',
    displayname: 'Red White & Booze',
    shortdescription: 'Country Bar Songs',
    progtypetitle: 'Music',
    genretitle: 'Country',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/redwhite&booze',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1407527688119&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 350,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 350,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'siriuslysinatra',
    displayname: 'Siriusly Sinatra',
    shortdescription: 'Standards By Sinatra & More',
    progtypetitle: 'Music',
    genretitle: 'Jazz/Standards',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriuslysinatra',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795972&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 71,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 71,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9206',
    displayname: 'Washington Redskins',
    shortdescription: 'Redskins Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/washingtonredskins',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283892628651&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 831,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 831,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9316',
    displayname: 'Philadelphia Flyers',
    shortdescription: 'Flyers Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NHL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/philadelphiaflyers',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283900813899&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 941,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 941,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8316',
    displayname: 'Sports Play-by-Play 211',
    shortdescription: 'Sports Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay211',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 211,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 211,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9415',
    displayname: 'Road Trip Radio',
    shortdescription: 'Music to Drive to!',
    progtypetitle: 'Music',
    genretitle: 'Pop',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/roadtripradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293945304767&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 301,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 301,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8333',
    displayname: 'MLB Network Radio™',
    shortdescription: '24/7 MLB® Talk & Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'Sports',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/mlbnetworkradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795463&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 89,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 89,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'elvisradio',
    displayname: 'Elvis Radio',
    shortdescription: 'Elvis 24/7 Live from Graceland',
    progtypetitle: 'Music',
    genretitle: 'Rock',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/elvisradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009734545&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 19,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 19,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'bluecollarcomedy',
    displayname: 'Jeff & Larry\'s Comedy Roundup',
    shortdescription: 'Great American Comedy-XL',
    progtypetitle: 'Talk & Entertainment',
    genretitle: 'Comedy',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/jeff&larryscomedyroundup',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1428904025644&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 97,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 97,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9233',
    displayname: 'Baltimore Orioles',
    shortdescription: 'Orioles Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/baltimoreoriolesonline',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283888344765&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 842,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 842,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9295',
    displayname: 'Washington Wizards',
    shortdescription: 'Wizards Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NBA Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nbawas',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293939023315&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 909,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 909,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9442',
    displayname: 'VOLUME',
    shortdescription: 'Music Talk That Rocks',
    progtypetitle: 'Talk & Entertainment',
    genretitle: 'Entertainment',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/volume',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293948565726&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 106,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 106,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9446',
    displayname: 'The Beatles Channel',
    shortdescription: 'The Fab Four, 24/8',
    progtypetitle: 'Music',
    genretitle: 'Rock',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/thebeatleschannel',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1496905137600&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 18,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 18,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9275',
    displayname: 'Golden State Warriors',
    shortdescription: 'Warriors Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NBA Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nbags',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293938969231&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 889,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 889,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9334',
    displayname: 'Sports Play-by-Play 990',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay990',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 990,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 990,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9305',
    displayname: 'Detroit Red Wings',
    shortdescription: 'Red Wings Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NHL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/detroitredwings',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283900804860&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 930,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 930,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'thebeat',
    displayname: 'BPM',
    shortdescription: 'Electronic Dance Music Hits',
    progtypetitle: 'Music',
    genretitle: 'Dance/Electronic',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/bpm',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009733019&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 51,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 51,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8329',
    displayname: 'NBA Play-by-Play 217',
    shortdescription: 'NFL, NASCAR, college sports and more',
    progtypetitle: 'Sports',
    genretitle: 'NBA Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nbaplaybyplay217',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795487&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 217,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 217,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '8254',
    displayname: 'ESPN Xtra',
    shortdescription: 'SportsCenter/Finebaum/PTI',
    progtypetitle: 'Sports',
    genretitle: 'Sports',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/espnxtra',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009794352&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 81,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 81,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8259',
    displayname: 'Canada Laughs',
    shortdescription: 'Canadian Comedy Uncensored XL',
    progtypetitle: 'Talk & Entertainment',
    genretitle: 'Comedy',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/laughattack',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283931200315&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 168,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 168,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9473',
    displayname: 'CBS Sports Radio',
    shortdescription: 'Sports Talk, Jim Rome',
    progtypetitle: 'Sports',
    genretitle: 'Sports',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/cbssportsradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293957662439&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 206,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 206,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'thevault',
    displayname: 'Deep Tracks',
    shortdescription: 'Deep Classic Rock',
    progtypetitle: 'Music',
    genretitle: 'Rock',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/deeptracks',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009734270&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 27,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 27,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9284',
    displayname: 'New Orleans Pelicans',
    shortdescription: 'Pelicans Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NBA Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nbano',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1387828191607&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 898,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 898,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9176',
    displayname: 'SiriusXM Comes Alive!',
    shortdescription: 'Live Classic Rock',
    progtypetitle: 'Music',
    genretitle: 'Rock',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxmcomesalive',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283879182276&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 316,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 316,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8248',
    displayname: 'SiriusXM Scoreboard',
    shortdescription: 'Final Sports Scores and Info',
    progtypetitle: 'More',
    genretitle: 'Canadian',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxmscoreboard',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1512384679233&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 172,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 172,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'espndeportes',
    displayname: 'ESPN Deportes',
    shortdescription: 'Latino Sports Talk & PXP',
    progtypetitle: 'Sports',
    genretitle: 'Sports',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/espndeportes',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283877698316&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 468,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 468,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9441',
    displayname: 'Sports Play-by-Play 391',
    shortdescription: 'Sports play by play',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay391',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1478623576618&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 391,
        packages: {
          siriusxmallaccess: true
        }
      },
      online: {
        number: 391,
        packages: {
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9314',
    displayname: 'New York Rangers',
    shortdescription: 'Rangers Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NHL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/newyorkrangers',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283900813463&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 939,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 939,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9451',
    displayname: 'Indie 1.0',
    shortdescription: 'First-generation Indie Rock',
    progtypetitle: 'Music',
    genretitle: 'Rock',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/indie10',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293952999090&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 714,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 714,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9132',
    displayname: 'Korea Today',
    shortdescription: 'Korean Music & News',
    progtypetitle: 'More',
    genretitle: 'More',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/koreatoday',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283881513173&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 144,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 144,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'cnbc',
    displayname: 'CNBC',
    shortdescription: 'CNBC Simulcast',
    progtypetitle: 'News & Issues',
    genretitle: 'News/Public Radio',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/cnbc',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009734077&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 112,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 112,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9254',
    displayname: 'San Francisco Giants',
    shortdescription: 'Giants Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sanfranciscogiantsonline',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283888412605&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 863,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 863,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'siriusleft',
    displayname: 'SiriusXM Progress',
    shortdescription: 'Progressive Talk',
    progtypetitle: 'News & Issues',
    genretitle: 'Politics/Issues',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxmprogress',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283936029945&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 127,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 127,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9293',
    displayname: 'Toronto Raptors',
    shortdescription: 'Raptors Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NBA Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nbator',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293939020590&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 907,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 907,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'jazzcafe',
    displayname: 'Watercolors',
    shortdescription: 'Smooth/Contemporary Jazz',
    progtypetitle: 'Music',
    genretitle: 'Jazz/Standards',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/watercolors',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796192&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 66,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 66,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'howardstern100',
    displayname: 'Howard 100',
    shortdescription: 'The Howard Stern Show–XL',
    progtypetitle: 'Howard Stern',
    genretitle: 'Howard Stern',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/howard100',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1428889287139&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 100,
        packages: {
          siriusxmallaccess: true
        }
      },
      online: {
        number: 100,
        packages: {
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9385',
    displayname: 'SiriusXM NBA Radio',
    shortdescription: '24/7 NBA Talk & Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'Sports',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxmnba',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1384286778849&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 86,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 86,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9469',
    displayname: 'Kevin Hart\'s Laugh Out Loud Radio',
    shortdescription: 'Comedy & Shows w/Kevin Hart-XL',
    progtypetitle: 'Talk & Entertainment',
    genretitle: 'Comedy',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/kevinhartslaughoutloudradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293956731621&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 96,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 96,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8373',
    displayname: 'XM Preview',
    shortdescription: 'XM Preview',
    progtypetitle: 'Talk & Entertainment',
    genretitle: 'Entertainment',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/xmpreview',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 1,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 1,
        packages: {
          SiriusXMSelect: false,
          SiriusXMMostlyMusic: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9152',
    displayname: 'Cincinnati Bengals',
    shortdescription: 'Bengals Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/cincinnatibengals',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283892889699&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 806,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 806,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9304',
    displayname: 'Dallas Stars',
    shortdescription: 'Stars Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NHL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/dallasstars',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283900594437&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 929,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 929,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9398',
    displayname: 'SiriusXM Limited Edition 4',
    shortdescription: 'Home for limited-run channels XL',
    progtypetitle: 'Music',
    genretitle: 'Hip-Hop/R&B',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxmlimitededition4',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293944377206&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 721,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 721,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8277',
    displayname: 'MLB Play-by-Play 186',
    shortdescription: 'Live MLB® Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/mlbplaybyplay186',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795439&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 186,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 186,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9164',
    displayname: 'New England Patriots',
    shortdescription: 'Patriots Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/newenglandpatriots',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283892894481&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 820,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 820,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9255',
    displayname: 'Seattle Mariners',
    shortdescription: 'Mariners Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/seattlemarinersonline',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283888413758&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 864,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 864,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8230',
    displayname: 'SiriusXM Rush',
    shortdescription: 'Combat Sports & Entertainment',
    progtypetitle: 'Sports',
    genretitle: 'Sports',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxmrush',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293944188083&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 93,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 93,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9280',
    displayname: 'Memphis Grizzlies',
    shortdescription: 'Grizzlies Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NBA Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nbamem',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293938974513&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 894,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 894,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'classicrewind',
    displayname: 'Classic Rewind',
    shortdescription: '\'70s/\'80s Classic Rock',
    progtypetitle: 'Music',
    genretitle: 'Rock',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/classicrewind',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009734045&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 25,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 25,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9168',
    displayname: 'Oakland Raiders',
    shortdescription: 'Raiders Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/oaklandraiders',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283892899965&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 824,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 824,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9240',
    displayname: 'Detroit Tigers',
    shortdescription: 'Tigers Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/detroittigersonline',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283888347303&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 849,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 849,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'siriusgold',
    displayname: '\'50s on 5',
    shortdescription: '\'50s Pop Hits',
    progtypetitle: 'Music',
    genretitle: 'Pop',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/50son5',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283872666703&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 5,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 5,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9323',
    displayname: 'Vancouver Canucks',
    shortdescription: 'Canucks Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NHL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/vancouvercanucks',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283900671441&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 947,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 947,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9330',
    displayname: 'Sports Play-by-Play 986',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay986',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 986,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 986,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'ewtnglobal',
    displayname: 'EWTN Radio',
    shortdescription: 'Solid Catholic Talk',
    progtypetitle: 'Talk & Entertainment',
    genretitle: 'Religion',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/ewtnradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283872583701&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 130,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 130,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8281',
    displayname: 'Big 12 Play-by-Play 199',
    shortdescription: 'Live Big 12 Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'College Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/big12playbyplay199',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1397562259489&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 199,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 199,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: 'siriuspops',
    displayname: 'SiriusXM Pops',
    shortdescription: 'Classical Pops',
    progtypetitle: 'Music',
    genretitle: 'Classical',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxmpops',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293944188342&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 755,
        packages: {
          SiriusXMSelect: false,
          SiriusXMMostlyMusic: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 755,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'thecatholicchannel',
    displayname: 'The Catholic Channel',
    shortdescription: 'Talk For Saints and Sinners',
    progtypetitle: 'Talk & Entertainment',
    genretitle: 'Religion',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/thecatholicchannel',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293944194945&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 129,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 129,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9236',
    displayname: 'Chicago White Sox',
    shortdescription: 'White Sox Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/chicagowhitesoxonline',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283888341579&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 845,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 845,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8238',
    displayname: 'SiriusXM Urban View',
    shortdescription: 'African-American Talk',
    progtypetitle: 'News & Issues',
    genretitle: 'Politics/Issues',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxmurbanview',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283931774694&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 126,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 126,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'cnnheadlinenews',
    displayname: 'HLN',
    shortdescription: 'HLN Simulcast',
    progtypetitle: 'News & Issues',
    genretitle: 'News/Public Radio',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/cnnheadlinenews',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293952998859&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 117,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 117,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9453',
    displayname: 'Vegas Golden Knights',
    shortdescription: 'Golden Knights Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NHL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/vegasgoldenknights',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293952999459&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 948,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 948,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8271',
    displayname: 'MLB Play-by-Play 180',
    shortdescription: 'Live MLB® Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/mlbplaybyplay180',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795439&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 180,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 180,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9422',
    displayname: 'Sports Play-by-Play 953',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay953',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 953,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 953,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9163',
    displayname: 'Minnesota Vikings',
    shortdescription: 'Vikings Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/minnesotavikings',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283892893963&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 819,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 819,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9375',
    displayname: 'Classic Rock Party',
    shortdescription: 'Non-Stop Classic Rock',
    progtypetitle: 'Music',
    genretitle: 'Rock',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/classicrockparty',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1383772052624&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 715,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 715,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9342',
    displayname: 'Holiday Traditions',
    shortdescription: 'Traditional Holiday Music',
    progtypetitle: 'Music',
    genretitle: 'Pop',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/holidaytraditionsonline',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283903127811&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 782,
        packages: {
          SiriusXMSelect: false,
          SiriusXMMostlyMusic: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 782,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8289',
    displayname: 'SEC Play-by-Play 192',
    shortdescription: 'Live SEC Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'College Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/secplaybyplay192',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795787&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 192,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 192,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: 'metropolitanopera',
    displayname: 'Met Opera Radio',
    shortdescription: 'Opera/Classical Voices',
    progtypetitle: 'Music',
    genretitle: 'Classical',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/metropolitanoperaradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283876750154&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 75,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 75,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9289',
    displayname: 'Phoenix Suns',
    shortdescription: 'Suns Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NBA Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nbapho',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293939016189&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 903,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 903,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9397',
    displayname: 'Sway\'s Universe',
    shortdescription: 'Sway\'s Lifestyle Channel XL',
    progtypetitle: 'Music',
    genretitle: 'Hip-Hop/R&B',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/swaysuniverse',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293943031185&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 720,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 720,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9157',
    displayname: 'Green Bay Packers',
    shortdescription: 'Packers Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/greenbaypackers',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283892892389&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 811,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 811,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9130',
    displayname: 'HBCU',
    shortdescription: 'HBCU Excellence In Education',
    progtypetitle: 'More',
    genretitle: 'More',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/hbcu',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1383847444955&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 142,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 142,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9364',
    displayname: 'SiriusXM Silk',
    shortdescription: 'Smooth R&B Love Songs',
    progtypetitle: 'Music',
    genretitle: 'Hip-Hop/R&B',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxmsilk',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1383849277653&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 330,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 330,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9208',
    displayname: 'Sports Play-by-Play 971',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay971',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 971,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 971,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'siriusblues',
    displayname: 'BB King\'s Bluesville',
    shortdescription: 'B.B. King\'s Blues Channel',
    progtypetitle: 'Music',
    genretitle: 'Jazz/Standards',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/bbkingsbluesville',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283872668680&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 74,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 74,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9171',
    displayname: 'Los Angeles Chargers',
    shortdescription: 'Chargers Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/losangeleschargers',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293953022076&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 816,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 816,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'primecountry',
    displayname: 'Prime Country',
    shortdescription: '\'80s/\'90s Country Hits',
    progtypetitle: 'Music',
    genretitle: 'Country',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/primecountry',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1407527583226&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 58,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 58,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9134',
    displayname: 'La Politica Talk',
    shortdescription: 'Live Hispanic Talk',
    progtypetitle: 'More',
    genretitle: 'More',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/lapoliticatalk',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293949605946&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 153,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 153,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8270',
    displayname: 'MLB Play-by-Play 179',
    shortdescription: 'Live MLB® Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/mlbplaybyplay179',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795439&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 179,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 179,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9474',
    displayname: 'SiriusXM 30',
    shortdescription: 'SiriusXM 30',
    progtypetitle: 'Music',
    genretitle: 'Rock',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxm30',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293958750899&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 30,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 30,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9149',
    displayname: 'Buffalo Bills',
    shortdescription: 'Bills Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/buffalobills',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283892887318&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 803,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 803,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9159',
    displayname: 'Indianapolis Colts',
    shortdescription: 'Colts Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/indianapoliscolts',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283892893000&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 813,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 813,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9361',
    displayname: 'Velvet',
    shortdescription: 'Today’s Pop Vocalists',
    progtypetitle: 'Music',
    genretitle: 'Pop',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/velvet',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1383897655472&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 304,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 304,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8256',
    displayname: 'Verizon IndyCar Series',
    shortdescription: 'Verizon IndyCar®, Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/verizonindycarseries',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293953001974&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 209,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 209,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9428',
    displayname: 'Sports Play-by-Play 959',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay959',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 959,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 959,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9311',
    displayname: 'New Jersey Devils',
    shortdescription: 'Devils Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NHL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/newjerseydevils',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283900812474&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 937,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 937,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8246',
    displayname: 'Influence Franco',
    shortdescription: 'The New Indie Pop Alternative',
    progtypetitle: 'More',
    genretitle: 'Canadian',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/influencefranco',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283931461319&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 174,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 174,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8215',
    displayname: 'Escape',
    shortdescription: 'Easy Listening',
    progtypetitle: 'Music',
    genretitle: 'Jazz/Standards',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/escape',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009734680&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 69,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 69,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'outlawcountry',
    displayname: 'Outlaw Country',
    shortdescription: 'Rockin\' Country Rebels',
    progtypetitle: 'Music',
    genretitle: 'Country',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/outlawcountry',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795596&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 60,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 60,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9202',
    displayname: 'San Francisco 49ers',
    shortdescription: '49ers Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sanfrancisco49ers',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283892902630&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 827,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 827,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'bbcworld',
    displayname: 'BBC World Service',
    shortdescription: 'World News',
    progtypetitle: 'News & Issues',
    genretitle: 'News/Public Radio',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/bbcworldservice',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283872668619&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 120,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 120,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9273',
    displayname: 'Denver Nuggets',
    shortdescription: 'Nuggets Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NBA Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nbaden',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293938969032&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 887,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 887,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9399',
    displayname: 'SiriusXM Limited Edition 5',
    shortdescription: 'Home for limited-run channels',
    progtypetitle: 'Music',
    genretitle: 'Hip-Hop/R&B',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxmlimitededition5',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293944377324&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 726,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 726,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'siriusstars',
    displayname: 'SiriusXM Stars',
    shortdescription: 'Jenny McCarthy/Dr Laura/MoreXL',
    progtypetitle: 'Talk & Entertainment',
    genretitle: 'Entertainment',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxmstars',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795898&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 109,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 109,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'siriussportsaction',
    displayname: 'ESPNU Radio',
    shortdescription: 'College Sports Talk/PXP',
    progtypetitle: 'Sports',
    genretitle: 'Sports',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/espnuradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293953774863&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 84,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 84,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9201',
    displayname: 'Seattle Seahawks',
    shortdescription: 'Seahawks Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/seattleseahawks',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283892894228&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 828,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 828,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9413',
    displayname: 'SiriusXM Turbo',
    shortdescription: '\'90s/2000s Hard Rock - XL',
    progtypetitle: 'Music',
    genretitle: 'Rock',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxmturbo',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293944195400&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 41,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 41,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9188',
    displayname: 'Caricia',
    shortdescription: 'Ballads in Spanish & English',
    progtypetitle: 'Music',
    genretitle: 'Latino',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/caricia',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283879426058&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 762,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 762,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9369',
    displayname: 'FOX Business',
    shortdescription: 'FOX Business Simulcast',
    progtypetitle: 'News & Issues',
    genretitle: 'News/Public Radio',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/foxbusiness',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1420522250427&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 113,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 113,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'heartandsoul',
    displayname: 'Heart & Soul',
    shortdescription: 'Adult R&B Hits',
    progtypetitle: 'Music',
    genretitle: 'Hip-Hop/R&B',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/heartandsoul',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795267&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 48,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 48,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8290',
    displayname: 'Sports Play-by-Play 203',
    shortdescription: 'Sports Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay203',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 203,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 203,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9278',
    displayname: 'Los Angeles Clippers',
    shortdescription: 'Clippers Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NBA Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nbalac',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293938973435&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 892,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 892,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8321',
    displayname: 'NFL Play-by-Play 229',
    shortdescription: 'Home for every NFL game',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nflplaybyplay229',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293940121536&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 229,
        packages: {
          siriusxmallaccess: true
        }
      },
      online: {
        number: 229,
        packages: {
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9170',
    displayname: 'Pittsburgh Steelers',
    shortdescription: 'Steelers Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/pittsburghsteelers',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283892903838&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 826,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 826,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8272',
    displayname: 'MLB Play-by-Play 181',
    shortdescription: 'Live MLB® Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/mlbplaybyplay181',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795439&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 181,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 181,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9238',
    displayname: 'Cleveland Indians',
    shortdescription: 'Indians Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/clevelandindiansonline',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283888345201&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 847,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 847,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'purejazz',
    displayname: 'Real Jazz',
    shortdescription: 'Classic Jazz',
    progtypetitle: 'Music',
    genretitle: 'Jazz/Standards',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/realjazz',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795749&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 67,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 67,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9321',
    displayname: 'Tampa Bay Lightning',
    shortdescription: 'Lightning Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NHL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/tampabaylightning',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283900666572&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 945,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 945,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9279',
    displayname: 'Los Angeles Lakers',
    shortdescription: 'Lakers Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NBA Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nbalal',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293938973792&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 893,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 893,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8283',
    displayname: 'Sports Play-by-Play 201',
    shortdescription: 'Sports Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay201',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 201,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 201,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9151',
    displayname: 'Chicago Bears',
    shortdescription: 'Bears Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/chicagobears',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283892889405&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 805,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 805,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9419',
    displayname: 'ONEderland',
    shortdescription: 'One-Hit Wonders, 24/7',
    progtypetitle: 'Music',
    genretitle: 'Pop',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/onederland',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293947942889&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 702,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 702,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9459',
    displayname: 'SiriusXM 375',
    shortdescription: 'SiriusXM 375',
    progtypetitle: 'Sports',
    genretitle: 'Sports',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxm375',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293953003155&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 375,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 375,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'shade45',
    displayname: 'Shade 45',
    shortdescription: 'Eminem\'s Hip-Hop Channel XL',
    progtypetitle: 'Music',
    genretitle: 'Hip-Hop/R&B',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/shade45',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795793&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 45,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 45,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9191',
    displayname: 'La Kueva',
    shortdescription: 'Latin Rock',
    progtypetitle: 'Music',
    genretitle: 'Latino',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/lakueva',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283879393144&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 768,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 768,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9341',
    displayname: 'SiriusXM FC',
    shortdescription: 'Soccer Talk & Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'Sports',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxmfc',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283902520369&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 157,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 157,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9129',
    displayname: 'HUR Voices',
    shortdescription: 'Real talk with real people',
    progtypetitle: 'More',
    genretitle: 'More',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/hurvoices',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283880618728&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 141,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 141,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8280',
    displayname: 'MLB Play-by-Play 189',
    shortdescription: 'Live MLB® Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/mlbplaybyplay189',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795439&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 189,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 189,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9359',
    displayname: 'Business Radio',
    shortdescription: 'Business Powered By Wharton',
    progtypetitle: 'Talk & Entertainment',
    genretitle: 'Entertainment',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/businessradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293938948954&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 111,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 111,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'rockvelours',
    displayname: 'Ici Musique FrancoCountry',
    shortdescription: 'Francophone Country Folk',
    progtypetitle: 'More',
    genretitle: 'Canadian',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/icimusiquefrancocountry',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1407527218853&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 166,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 166,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9410',
    displayname: 'FOX News Headlines 24/7',
    shortdescription: 'FOX News Headlines, 24/7',
    progtypetitle: 'News & Issues',
    genretitle: 'News/Public Radio',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/foxnewsheadlines',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293944186738&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 115,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 115,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'coffeehouse',
    displayname: 'The Coffee House',
    shortdescription: 'Acoustic/Singer-Songwriters',
    progtypetitle: 'Music',
    genretitle: 'Pop',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/thecoffeehouse',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796065&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 14,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 14,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9400',
    displayname: 'SiriusXM Limited Edition 6',
    shortdescription: 'Home for limited-run channels',
    progtypetitle: 'Music',
    genretitle: 'Dance/Electronic',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxmlimitededition6',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293944377709&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 730,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 730,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9166',
    displayname: 'New York Giants',
    shortdescription: 'Giants Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/newyorkgiants',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283892894964&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 822,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 822,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'premiereplus',
    displayname: 'Ici Première',
    shortdescription: 'Radio-Canada News & Info',
    progtypetitle: 'More',
    genretitle: 'Canadian',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/icipremiere',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1402060447112&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 170,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 170,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'estreetradio',
    displayname: 'E Street Radio',
    shortdescription: 'Bruce Springsteen, 24/7',
    progtypetitle: 'Music',
    genretitle: 'Rock',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/estreetradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009794972&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 20,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 20,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9270',
    displayname: 'Chicago Bulls',
    shortdescription: 'Bulls Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NBA Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nbachi',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293938960748&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 884,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 884,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9430',
    displayname: 'Sports Play-by-Play 380',
    shortdescription: 'Sports play by play',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay380',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293948975890&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 380,
        packages: {
          siriusxmallaccess: true
        }
      },
      online: {
        number: 380,
        packages: {
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9260',
    displayname: 'Washington Nationals',
    shortdescription: 'Nationals Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/washingtonnationalsonline',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283888418606&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 869,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 869,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'jamon',
    displayname: 'Jam On',
    shortdescription: 'Jam Bands',
    progtypetitle: 'Music',
    genretitle: 'Rock',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/jamon',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795336&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 29,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 29,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9367',
    displayname: 'RURAL RADIO',
    shortdescription: 'Agriculture/Western Lifestyle',
    progtypetitle: 'News & Issues',
    genretitle: 'News/Public Radio',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/ruralradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283934867354&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 147,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 147,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9301',
    displayname: 'Calgary Flames',
    shortdescription: 'Flames Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NHL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/calgaryflames',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283900824917&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 924,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 924,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'doctorradio',
    displayname: 'Doctor Radio',
    shortdescription: 'Real Doctors, Real People',
    progtypetitle: 'Talk & Entertainment',
    genretitle: 'Entertainment',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/doctorradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009734286&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 110,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 110,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'laughbreak',
    displayname: 'Laugh USA',
    shortdescription: 'Comedy For The Entire Family',
    progtypetitle: 'Talk & Entertainment',
    genretitle: 'Comedy',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/laughusa',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795366&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 98,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 98,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9373',
    displayname: '80s/90s Pop',
    shortdescription: '\'80s & \'90s Party Hits',
    progtypetitle: 'Music',
    genretitle: 'Pop',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/80s90sparty',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1383691711047&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 705,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 705,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9189',
    displayname: 'Luna',
    shortdescription: 'Latin Jazz',
    progtypetitle: 'Music',
    genretitle: 'Latino',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/luna',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283879393816&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 766,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 766,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9365',
    displayname: 'Utopia',
    shortdescription: '\'90s/2000s Dance Hits',
    progtypetitle: 'Music',
    genretitle: 'Dance/Electronic',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/utopia',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293938947187&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 341,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 341,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9250',
    displayname: 'Oakland Athletics',
    shortdescription: 'Athletics Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/oaklandathleticsonline',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283888360045&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 859,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 859,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9294',
    displayname: 'Utah Jazz',
    shortdescription: 'Jazz Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NBA Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nbauth',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293939020764&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 908,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 908,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8268',
    displayname: 'MLB Play-by-Play 177',
    shortdescription: 'Live MLB® Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/mlbplaybyplay177',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795439&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 177,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 177,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9411',
    displayname: 'SiriusXM 145',
    shortdescription: 'SiriusXM 145',
    progtypetitle: 'More',
    genretitle: 'More',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxm145',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293944194837&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 145,
        packages: {
          SiriusXMSelect: false,
          SiriusXMMostlyMusic: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 145,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9239',
    displayname: 'Colorado Rockies',
    shortdescription: 'Rockies Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/coloradorockiesonline',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283888346523&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 848,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 848,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9475',
    displayname: 'Dwight Yoakam and The Bakersfield B',
    shortdescription: 'Dwight Yoakam\'s Music Channel',
    progtypetitle: 'Music',
    genretitle: 'Country',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/dwightyoakam',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293958748164&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 349,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 349,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9212',
    displayname: 'Sports Play-by-Play 975',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay975',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 975,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 975,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9303',
    displayname: 'Colorado Avalanche',
    shortdescription: 'Avalanche Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NHL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/coloradoavalanche',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283900808270&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 927,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 927,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9237',
    displayname: 'Cincinnati Reds',
    shortdescription: 'Reds Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/cincinnatiredsonline',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283888344403&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 846,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 846,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'theroadhouse',
    displayname: 'Willie\'s Roadhouse',
    shortdescription: 'Willie\'s Classic Country',
    progtypetitle: 'Music',
    genretitle: 'Country',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/williesroadhouse',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283876490534&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 59,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 59,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8207',
    displayname: 'The Loft',
    shortdescription: 'Eclectic Rock',
    progtypetitle: 'Music',
    genretitle: 'Rock',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/theloft',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796104&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 710,
        packages: {
          SiriusXMSelect: false,
          SiriusXMMostlyMusic: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 710,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9322',
    displayname: 'Toronto Maple Leafs',
    shortdescription: 'Maple Leafs Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NHL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/torontomapleleafs',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283900668990&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 946,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 946,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8326',
    displayname: 'NFL Play-by-Play 234',
    shortdescription: 'Home for every NFL game',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nflplaybyplay234',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293940121536&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 234,
        packages: {
          siriusxmallaccess: true
        }
      },
      online: {
        number: 234,
        packages: {
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9288',
    displayname: 'Philadelphia 76ers',
    shortdescription: '76ers Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NBA Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nbaphi',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293939016462&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 902,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 902,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8279',
    displayname: 'MLB Play-by-Play 188',
    shortdescription: 'Live MLB® Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/mlbplaybyplay188',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795439&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 188,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 188,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9401',
    displayname: 'SiriusXM Limited Edition 7',
    shortdescription: 'Home for limited-run channels',
    progtypetitle: 'Music',
    genretitle: 'Country',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxmlimitededition7',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293944377817&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 742,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 742,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9185',
    displayname: 'Flow Nación',
    shortdescription: 'Latin Urban Music',
    progtypetitle: 'Music',
    genretitle: 'Latino',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/flownacion',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283879390165&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 765,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 765,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9328',
    displayname: 'Sports Play-by-Play 984',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay984',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 984,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 984,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8284',
    displayname: 'ACC Play-by-Play 193',
    shortdescription: 'Live ACC Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'College Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/accplaybyplay193',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1467402485453&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 193,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 193,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9181',
    displayname: 'Carlin\'s Corner',
    shortdescription: 'One & Only George Carlin XL',
    progtypetitle: 'Talk & Entertainment',
    genretitle: 'Comedy',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/carlinscorner',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283879167234&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 770,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 770,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8211',
    displayname: 'Cinemagic',
    shortdescription: 'Movie Soundtracks and More',
    progtypetitle: 'Music',
    genretitle: 'Jazz/Standards',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/cinemagic',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009734029&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 750,
        packages: {
          SiriusXMSelect: false,
          SiriusXMMostlyMusic: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 750,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'faction',
    displayname: 'Faction Punk',
    shortdescription: 'Punk Rock 24/7 XL',
    progtypetitle: 'Music',
    genretitle: 'Rock',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/factionpunk',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293954973812&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 314,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 314,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9242',
    displayname: 'Kansas City Royals',
    shortdescription: 'Royals Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/kansascityroyalsonline',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283888347247&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 851,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 851,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9214',
    displayname: 'Sports Play-by-Play 977',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay977',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 977,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 977,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9431',
    displayname: 'Sports Play-by-Play 381',
    shortdescription: 'Sports play by play',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay381',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293948975989&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 381,
        packages: {
          siriusxmallaccess: true
        }
      },
      online: {
        number: 381,
        packages: {
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '8216',
    displayname: 'Kids Place Live',
    shortdescription: 'Kids\' Music',
    progtypetitle: 'Talk & Entertainment',
    genretitle: 'Kids',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/kidsplacelive',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795354&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 78,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 78,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'howardstern101',
    displayname: 'Howard 101',
    shortdescription: 'The World of Howard Stern-XL',
    progtypetitle: 'Howard Stern',
    genretitle: 'Howard Stern',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/howard101',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1428889327092&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 101,
        packages: {
          siriusxmallaccess: true
        }
      },
      online: {
        number: 101,
        packages: {
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9450',
    displayname: 'PopRocks',
    shortdescription: 'Rock & Pop from the \'90s & 2Ks',
    progtypetitle: 'Music',
    genretitle: 'Pop',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/poprocks',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293953000321&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 17,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 17,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8288',
    displayname: 'SEC Play-by-Play 191',
    shortdescription: 'Live SEC Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'College Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/secplaybyplay191',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795787&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 191,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 191,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9229',
    displayname: 'Sports Play-by-Play 969',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay969',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 969,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 969,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8276',
    displayname: 'MLB Play-by-Play 185',
    shortdescription: 'Live MLB® Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/mlbplaybyplay185',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795439&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 185,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 185,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9449',
    displayname: 'Triumph',
    shortdescription: 'Dave Ramsey and More',
    progtypetitle: 'News & Issues',
    genretitle: 'Politics/Issues',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/triumph',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293953001709&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 132,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 132,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9186',
    displayname: 'Águila',
    shortdescription: 'Regional Mexican Music',
    progtypetitle: 'Music',
    genretitle: 'Latino',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/aguila',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283879362568&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 761,
        packages: {
          SiriusXMSelect: false,
          SiriusXMMostlyMusic: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 761,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9408',
    displayname: 'SiriusXM Comedy Greats',
    shortdescription: 'All-Time Greatest Comedians XL',
    progtypetitle: 'Talk & Entertainment',
    genretitle: 'Comedy',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxmcomedygreats',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293944200792&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 94,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 94,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9204',
    displayname: 'Tampa Bay Buccaneers',
    shortdescription: 'Buccaneers Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/tampabaybuccaneers',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283892644811&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 829,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 829,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9286',
    displayname: 'Oklahoma City Thunder',
    shortdescription: 'Thunder Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NBA Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nbaokc',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293939015089&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 900,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 900,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9135',
    displayname: 'En Vivo',
    shortdescription: 'Where Music Is Born',
    progtypetitle: 'More',
    genretitle: 'More',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/envivo',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283881512245&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 152,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 152,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8273',
    displayname: 'MLB Play-by-Play 182',
    shortdescription: 'Live MLB® Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/mlbplaybyplay182',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795439&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 182,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 182,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9308',
    displayname: 'Los Angeles Kings',
    shortdescription: 'Kings Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NHL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/losangeleskings',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283900806577&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 933,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 933,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9232',
    displayname: 'Atlanta Braves',
    shortdescription: 'Braves Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/atlantabravesonline',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283888341585&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 841,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 841,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9227',
    displayname: 'Sports Play-by-Play 967',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay967',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 967,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 967,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9340',
    displayname: 'Y2Kountry',
    shortdescription: '2000s Country Hits',
    progtypetitle: 'Music',
    genretitle: 'Country',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/y2kountry',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283932530313&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 61,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 61,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9259',
    displayname: 'Toronto Blue Jays',
    shortdescription: 'Blue Jays Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/torontobluejaysonline',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283888410914&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 868,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 868,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'hairnation',
    displayname: 'Hair Nation',
    shortdescription: '\'80s Hair Bands',
    progtypetitle: 'Music',
    genretitle: 'Rock',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/hairnation',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795241&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 39,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 39,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9443',
    displayname: 'Ramsey Media Channel',
    shortdescription: 'Your Trusted Voice on Money',
    progtypetitle: 'Talk & Entertainment',
    genretitle: 'Entertainment',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/ramseymedia',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1512382203984&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 792,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 792,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9251',
    displayname: 'Philadelphia Phillies',
    shortdescription: 'Phillies Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/philadelphiaphilliesonline',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283888372685&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 860,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 860,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'chicagostlouis',
    displayname: 'Chicago/Detroit/Dallas-Ft. Worth',
    shortdescription: 'Chicago/Detroit/Dallas-F',
    progtypetitle: 'News & Issues',
    genretitle: 'Traffic/Weather',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/chicagodetroitdallasftworth',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283876544320&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 135,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 135,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9468',
    displayname: 'North Americana',
    shortdescription: 'Americana Music',
    progtypetitle: 'More',
    genretitle: 'Canadian',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/northamericana',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293956731129&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 359,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 359,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9432',
    displayname: 'Sports Play-by-Play 382',
    shortdescription: 'Sports play by play',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay382',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293948976060&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 382,
        packages: {
          siriusxmallaccess: true
        }
      },
      online: {
        number: 382,
        packages: {
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9215',
    displayname: 'Sports Play-by-Play 978',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay978',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 978,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 978,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9324',
    displayname: 'Washington Capitals',
    shortdescription: 'Capitals Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NHL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/washingtoncapitals',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283900674122&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 949,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 949,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9470',
    displayname: 'Marky Ramone\'s Punk Rock Blitzkrieg',
    shortdescription: 'Marky Ramone\'s Classic Punk',
    progtypetitle: 'Music',
    genretitle: 'Rock',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/markyramonesblitzkrieg',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293958749858&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 712,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 712,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9175',
    displayname: 'RockBar',
    shortdescription: 'Rock & Roll Jukebox Songs',
    progtypetitle: 'Music',
    genretitle: 'Rock',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/rockbar',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283879179247&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 313,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 313,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8237',
    displayname: 'C-SPAN Radio',
    shortdescription: 'C-SPAN Live Simulcast',
    progtypetitle: 'News & Issues',
    genretitle: 'News/Public Radio',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/cspan',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009734229&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 455,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 455,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9378',
    displayname: 'Oldies Party',
    shortdescription: 'Party Songs from the 50s & 60s',
    progtypetitle: 'Music',
    genretitle: 'Pop',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/oldiesparty',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1383696398835&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 703,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 703,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9290',
    displayname: 'Portland Trail Blazers',
    shortdescription: 'Trail Blazers Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NBA Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nbapor',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293939017346&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 904,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 904,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9169',
    displayname: 'Philadelphia Eagles',
    shortdescription: 'Eagles Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/philadelphiaeagles',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283892901559&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 825,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 825,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9243',
    displayname: 'Los Angeles Angels',
    shortdescription: 'Angels Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/losangelesangelsonline',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283888346825&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 852,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 852,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8315',
    displayname: 'Sports Play-by-Play 210',
    shortdescription: 'Sports Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay210',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 210,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 210,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: 'soultown',
    displayname: 'Soul Town',
    shortdescription: 'Classic Soul/Motown',
    progtypetitle: 'Music',
    genretitle: 'Hip-Hop/R&B',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/soultown',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795979&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 49,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 49,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8185',
    displayname: 'SiriusXM® NHL Network Radio™',
    shortdescription: '24/7 NHL® Talk & Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'Sports',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxmnhlnetworkradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1402043245354&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 91,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 91,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9207',
    displayname: 'Verizon IndyCar Series',
    shortdescription: 'Verizon IndyCar®, Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay970',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 970,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 970,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9268',
    displayname: 'Boston Celtics',
    shortdescription: 'Celtics Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NBA Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nbabos',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293938945061&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 881,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 881,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'bandeapart',
    displayname: 'CBC Country',
    shortdescription: 'New Canadian Country',
    progtypetitle: 'More',
    genretitle: 'Canadian',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/cbccountry',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1439177623884&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 171,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 171,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8244',
    displayname: 'The Verge',
    shortdescription: 'New & Emerging Indie/Alt-Rock',
    progtypetitle: 'More',
    genretitle: 'Canadian',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/theverge',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283932530208&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 173,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 173,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9190',
    displayname: 'Rumbón',
    shortdescription: 'Classic Salsa',
    progtypetitle: 'Music',
    genretitle: 'Latino',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/rumbon',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283881545271&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 767,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 767,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'indietalk',
    displayname: 'POTUS Politics',
    shortdescription: 'Non-Partisan Political Talk',
    progtypetitle: 'News & Issues',
    genretitle: 'Politics/Issues',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/potus',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795643&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 124,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 124,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'siriuslove',
    displayname: 'SiriusXM Love',
    shortdescription: 'Love Songs',
    progtypetitle: 'Music',
    genretitle: 'Pop',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxmlove',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293944187368&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 70,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 70,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9235',
    displayname: 'Chicago Cubs',
    shortdescription: 'Cubs Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/chicagocubsonline',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283888399655&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 844,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 844,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9167',
    displayname: 'New York Jets',
    shortdescription: 'Jets Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/newyorkjets',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283892896140&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 823,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 823,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'cbcradioone',
    displayname: 'CBC Radio One',
    shortdescription: 'Canada\'s #1 Radio News Source',
    progtypetitle: 'News & Issues',
    genretitle: 'News/Public Radio',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/cbcradioone',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283932527788&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 169,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 169,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'energie2',
    displayname: 'Attitude Franco',
    shortdescription: 'The New Rock Alternative',
    progtypetitle: 'More',
    genretitle: 'Canadian',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/attitudefranco',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283931294876&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 759,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 759,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8302',
    displayname: 'NBA Play-by-Play 212',
    shortdescription: 'Live NBA Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NBA Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nbaplaybyplay212',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795487&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 212,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 212,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9131',
    displayname: 'BYUradio',
    shortdescription: 'Talk About Good',
    progtypetitle: 'More',
    genretitle: 'More',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/byuradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283877449444&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 143,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 143,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9402',
    displayname: 'SiriusXM Limited Edition 8',
    shortdescription: 'Home for limited-run channels',
    progtypetitle: 'Music',
    genretitle: 'Christian',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxmlimitededition8',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293944378212&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 745,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 745,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9226',
    displayname: 'Sports Play-by-Play 966',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay966',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 966,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 966,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9445',
    displayname: 'FOX Sports on SiriusXM',
    shortdescription: 'The Herd/Undisputed/Live Games',
    progtypetitle: 'Sports',
    genretitle: 'Sports',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/foxsports',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1512381589747&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 83,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 83,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9457',
    displayname: 'SiriusXM Pac-12 Radio',
    shortdescription: '24/7 Pac-12 Talk & Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'Sports',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxmpac12radio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293953003001&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 373,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 373,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8327',
    displayname: 'Sports Play-by-Play 235',
    shortdescription: 'NCAA, NFL, NBA, NHL, and More Sports',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay235',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 235,
        packages: {
          siriusxmallaccess: true
        }
      },
      online: {
        number: 235,
        packages: {
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '8287',
    displayname: 'SEC Play-by-Play 190',
    shortdescription: 'Live SEC Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'College Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/secplaybyplay190',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795787&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 190,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 190,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '8303',
    displayname: 'NBA Play-by-Play 213',
    shortdescription: 'Live NBA Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NBA Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nbaplaybyplay213',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795487&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 213,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 213,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '8205',
    displayname: '40s Junction',
    shortdescription: '\'40s Pop Hits/Big Band',
    progtypetitle: 'Music',
    genretitle: 'Jazz/Standards',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/40sjunction',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293944183968&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 73,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 73,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8323',
    displayname: 'NFL Play-by-Play 231',
    shortdescription: 'Home for every NFL game',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nflplaybyplay231',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293940121536&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 231,
        packages: {
          siriusxmallaccess: true
        }
      },
      online: {
        number: 231,
        packages: {
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '8278',
    displayname: 'MLB Play-by-Play 187',
    shortdescription: 'Live MLB® Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/mlbplaybyplay187',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795439&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 187,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 187,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9363',
    displayname: 'Jason Ellis',
    shortdescription: 'Jason Ellis Show Nonstop XL',
    progtypetitle: 'Talk & Entertainment',
    genretitle: 'Entertainment',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/jasonellis',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283935892544&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 791,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 791,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9339',
    displayname: 'SiriusXM FLY',
    shortdescription: '\'90s & 2000s Hip-Hop/R&B - XL',
    progtypetitle: 'Music',
    genretitle: 'Hip-Hop/R&B',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxmfly',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283928987312&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 47,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 47,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'siriuspatriot',
    displayname: 'SiriusXM Patriot',
    shortdescription: 'Conservative Talk',
    progtypetitle: 'News & Issues',
    genretitle: 'Politics/Issues',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxmpatriot',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1473228823270&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 125,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 125,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8245',
    displayname: 'Ici Musique Chansons',
    shortdescription: 'Francophone Pop',
    progtypetitle: 'More',
    genretitle: 'Canadian',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/icimusiquechansons',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1407524976297&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 163,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 163,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9247',
    displayname: 'Minnesota Twins',
    shortdescription: 'Twins Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/minnesotatwinsonline',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283888359726&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 856,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 856,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9154',
    displayname: 'Dallas Cowboys',
    shortdescription: 'Cowboys Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/dallascowboys',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283892890596&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 808,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 808,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9448',
    displayname: 'Vegas Stats & Information Network',
    shortdescription: 'Trusted Source For Gaming News',
    progtypetitle: 'Sports',
    genretitle: 'Sports',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/vegasstats',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293950787502&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 204,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 204,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9172',
    displayname: 'Canada Talks',
    shortdescription: 'Canadian Current Affairs',
    progtypetitle: 'More',
    genretitle: 'Canadian',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/canadatalks',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283931441519&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 167,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 167,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9338',
    displayname: 'Sports Play-by-Play 994',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay994',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 994,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 994,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'octane',
    displayname: 'Octane',
    shortdescription: 'New Hard Rock',
    progtypetitle: 'Music',
    genretitle: 'Rock',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/octane',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795576&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 37,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 37,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9366',
    displayname: 'KIDZ BOP Radio',
    shortdescription: 'Pop Hits Sung By Kids For Kids',
    progtypetitle: 'Talk & Entertainment',
    genretitle: 'Kids',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/kidzbopradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1381955229855&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 77,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 77,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8242',
    displayname: 'Z100/NY',
    shortdescription: 'Z100 plays all the hits',
    progtypetitle: 'More',
    genretitle: 'More',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/z100ny',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283877214230&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 12,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 12,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9471',
    displayname: 'Rock The Bells Radio',
    shortdescription: 'Classic Hip-Hop w/ LLCoolJ-XL',
    progtypetitle: 'Music',
    genretitle: 'Hip-Hop/R&B',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/rockthebellsradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293957659963&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 43,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 43,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'big80s',
    displayname: '\'80s on 8',
    shortdescription: '\'80s Pop Hits',
    progtypetitle: 'Music',
    genretitle: 'Pop',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/80son8',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283872667329&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 8,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 8,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8369',
    displayname: 'Vivid Radio',
    shortdescription: 'Superstars, Celebs, Hot Sex XL',
    progtypetitle: 'Talk & Entertainment',
    genretitle: 'Entertainment',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/vividradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1383848229353&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 415,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 415,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9209',
    displayname: 'Sports Play-by-Play 972',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay972',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 972,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 972,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8317',
    displayname: 'NFL Play-by-Play 225',
    shortdescription: 'Home for every NFL game',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nflplaybyplay225',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293940121536&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 225,
        packages: {
          siriusxmallaccess: true
        }
      },
      online: {
        number: 225,
        packages: {
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9372',
    displayname: '70s/80s Pop',
    shortdescription: '\'70s & \'80s Super Party Hits',
    progtypetitle: 'Music',
    genretitle: 'Pop',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/70s80spop',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1383344000831&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 704,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 704,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'foxnewschannel',
    displayname: 'FOX News Channel',
    shortdescription: 'FOX News Simulcast',
    progtypetitle: 'News & Issues',
    genretitle: 'News/Public Radio',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/foxnewschannel',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795140&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 114,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 114,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8274',
    displayname: 'MLB Play-by-Play 183',
    shortdescription: 'Live MLB® Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/mlbplaybyplay183',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795439&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 183,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 183,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9312',
    displayname: 'Nashville Predators',
    shortdescription: 'Predators Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NHL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nashvillepredators',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283900812955&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 936,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 936,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9433',
    displayname: 'Sports Play-by-Play 383',
    shortdescription: 'Sports play by play',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay383',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293948976136&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 383,
        packages: {
          siriusxmallaccess: true
        }
      },
      online: {
        number: 383,
        packages: {
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9241',
    displayname: 'Houston Astros',
    shortdescription: 'Astros Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/houstonastrosonline',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283888346825&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 850,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 850,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9319',
    displayname: 'San Jose Sharks',
    shortdescription: 'Sharks Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NHL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sanjosesharks',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283900658702&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 943,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 943,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'undergroundgarage',
    displayname: 'Underground Garage',
    shortdescription: 'Little Steven\'s Garage Rock',
    progtypetitle: 'Music',
    genretitle: 'Rock',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/undergroundgarage',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796174&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 21,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 21,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9165',
    displayname: 'New Orleans Saints',
    shortdescription: 'Saints Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/neworleanssaints',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283892894766&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 821,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 821,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8206',
    displayname: '\'90s on 9',
    shortdescription: '\'90s Pop Hits',
    progtypetitle: 'Music',
    genretitle: 'Pop',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/90son9',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283872667605&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 9,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 9,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9454',
    displayname: 'CNN International',
    shortdescription: 'News From Around The World',
    progtypetitle: 'News & Issues',
    genretitle: 'News/Public Radio',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/cnninternational',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293952997508&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 454,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 454,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8267',
    displayname: 'MLB Play-by-Play 176',
    shortdescription: 'Live MLB® Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/mlbplaybyplay176',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795439&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 176,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 176,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: 'praise',
    displayname: 'Kirk Franklin\'s Praise',
    shortdescription: 'Kirk Franklin\'s Gospel Channel',
    progtypetitle: 'Music',
    genretitle: 'Christian',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/kirkfranklinspraise',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795649&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 64,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 64,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9335',
    displayname: 'Sports Play-by-Play 991',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay991',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 991,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 991,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'thebridge',
    displayname: 'The Bridge',
    shortdescription: 'Mellow Classic Rock',
    progtypetitle: 'Music',
    genretitle: 'Rock',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/thebridge',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796053&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 32,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 32,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'siriusnflradio',
    displayname: 'SiriusXM NFL Radio',
    shortdescription: '24/7 NFL Talk & Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'Sports',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusnflradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283876463375&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 88,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 88,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9466',
    displayname: 'Telemundo',
    shortdescription: 'Spanish series, specials, more',
    progtypetitle: 'More',
    genretitle: 'More',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/telemundo',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1552838858573&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 469,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 469,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9358',
    displayname: 'Canadian Indigenous Peoples\' Radio',
    shortdescription: 'Canadian Indigenous Peoples\' Radio',
    progtypetitle: 'More',
    genretitle: 'Canadian',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/canadianindiginouspeoples',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1512380059609&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 165,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 165,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'broadwaysbest',
    displayname: 'On Broadway',
    shortdescription: 'Show Tunes',
    progtypetitle: 'Music',
    genretitle: 'Jazz/Standards',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/onbroadway',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795582&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 72,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 72,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9403',
    displayname: 'SiriusXM Limited Edition 9',
    shortdescription: 'Home for limited-run channels',
    progtypetitle: 'Music',
    genretitle: 'Jazz/Standards',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxmlimitededition9',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293944378689&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 752,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 752,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8285',
    displayname: 'ACC Play-by-Play 194',
    shortdescription: 'Live ACC Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'College Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/accplaybyplay194',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1467402485453&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 194,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 194,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9153',
    displayname: 'Cleveland Browns',
    shortdescription: 'Browns Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/clevelandbrowns',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283892890238&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 807,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 807,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9225',
    displayname: 'Sports Play-by-Play 965',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay965',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 965,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 965,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9277',
    displayname: 'Indiana Pacers',
    shortdescription: 'Pacers Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NBA Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nbaind',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293938970004&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 891,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 891,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8330',
    displayname: 'Big Ten Play-by-Play 196',
    shortdescription: 'Live Big 10 Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'College Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/bigtenplaybyplay196',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1397562600248&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 196,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 196,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9409',
    displayname: 'Radio Andy',
    shortdescription: 'Andy Cohen/Pop Culture/More XL',
    progtypetitle: 'Talk & Entertainment',
    genretitle: 'Entertainment',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/radioandy',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293944494033&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 102,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 102,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8282',
    displayname: 'Big 12 Play-by-Play 200',
    shortdescription: 'Live Big 12 Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'College Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/big12playbyplay200',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1397562259489&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 200,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 200,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9160',
    displayname: 'Jacksonville Jaguars',
    shortdescription: 'Jaguars Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/jacksonvillejaguars',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283892893213&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 814,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 814,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9351',
    displayname: 'Entertainment Weekly Radio',
    shortdescription: 'Entertainment Talk & News',
    progtypetitle: 'Talk & Entertainment',
    genretitle: 'Entertainment',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/entertainmentweeklyradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283931292966&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 105,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 105,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'spirit',
    displayname: 'The Message',
    shortdescription: 'Christian Pop & Rock',
    progtypetitle: 'Music',
    genretitle: 'Christian',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/themessage',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796110&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 63,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 63,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9269',
    displayname: 'Charlotte Hornets',
    shortdescription: 'Hornets Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NBA Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nbacha',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293938958520&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 883,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 883,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8331',
    displayname: 'Pac-12 Play-by-Play 197',
    shortdescription: 'Live Pac-12 Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'College Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/pac12playbyplay197',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1397562783691&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 197,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 197,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: 'hardattack',
    displayname: 'Liquid Metal',
    shortdescription: 'Heavy Metal-XL',
    progtypetitle: 'Music',
    genretitle: 'Rock',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/liquidmetal',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795372&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 40,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 40,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9394',
    displayname: 'Arizona Coyotes',
    shortdescription: 'Coyotes Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NHL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/arizonacoyotes',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1407527482947&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 921,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 921,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9467',
    displayname: 'Barstool Radio on SiriusXM',
    shortdescription: 'Satirical Sports/Men\'s Talk-XL',
    progtypetitle: 'Sports',
    genretitle: 'Sports',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/barstoolradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1540940805495&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 85,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 85,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9221',
    displayname: 'Sports Play-by-Play 961',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay961',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 961,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 961,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8208',
    displayname: 'Pop2K',
    shortdescription: '2000s Pop Hits',
    progtypetitle: 'Music',
    genretitle: 'Pop',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/pop2k',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795637&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 10,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 10,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'cbcradio3',
    displayname: 'CBC Radio 3',
    shortdescription: 'Canadian Indie Music First',
    progtypetitle: 'More',
    genretitle: 'Canadian',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/cbcradio3',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283932527016&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 162,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 162,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8275',
    displayname: 'MLB Play-by-Play 184',
    shortdescription: 'Live MLB® Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/mlbplaybyplay184',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795439&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 184,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 184,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9252',
    displayname: 'Pittsburgh Pirates',
    shortdescription: 'Pirates Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/pittsburghpiratesonline',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283888373627&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 861,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 861,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9244',
    displayname: 'Los Angeles Dodgers',
    shortdescription: 'Dodgers Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/losangelesdodgersonline',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283888346982&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 853,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 853,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9287',
    displayname: 'Orlando Magic',
    shortdescription: 'Magic Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NBA Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nbaorl',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293939015976&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 901,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 901,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'firstwave',
    displayname: '1st Wave',
    shortdescription: '\'80s Alternative/New Wave',
    progtypetitle: 'Music',
    genretitle: 'Rock',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/1stwave',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283872666386&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 33,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 33,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9213',
    displayname: 'Sports Play-by-Play 976',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay976',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 976,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 976,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9309',
    displayname: 'Minnesota Wild',
    shortdescription: 'Wild Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NHL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/minnesotawild',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283900809756&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 934,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 934,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'gratefuldead',
    displayname: 'Grateful Dead Channel',
    shortdescription: 'Grateful Dead, 24/7',
    progtypetitle: 'Music',
    genretitle: 'Rock',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/gratefuldead',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795232&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 23,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 23,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9434',
    displayname: 'Sports Play-by-Play 384',
    shortdescription: 'Sports play by play',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay384',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293948976307&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 384,
        packages: {
          siriusxmallaccess: true
        }
      },
      online: {
        number: 384,
        packages: {
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9447',
    displayname: 'The Emo Project',
    shortdescription: 'Emotionally Driven Alt Rock',
    progtypetitle: 'Music',
    genretitle: 'Rock',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/emoproject',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1496904784839&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 713,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 713,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9336',
    displayname: 'Sports Play-by-Play 992',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay992',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 992,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 992,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8305',
    displayname: 'NBA Play-by-Play 215',
    shortdescription: 'Live NBA Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NBA Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nbaplaybyplay215',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795487&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 215,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 215,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '8286',
    displayname: 'Big Ten Play-by-Play 195',
    shortdescription: 'Live Big 10 Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'College Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/bigtenplaybyplay195',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1397562600248&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 195,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 195,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9271',
    displayname: 'Cleveland Cavaliers',
    shortdescription: 'Cavaliers Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NBA Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nbacle',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293938963196&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 885,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 885,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9155',
    displayname: 'Denver Broncos',
    shortdescription: 'Broncos Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/denverbroncos',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283892890827&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 809,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 809,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8186',
    displayname: 'SiriusXM PGA TOUR Radio',
    shortdescription: '24/7 Golf Talk & Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'Sports',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxmpgatourradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283881796598&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 92,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 92,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9180',
    displayname: 'SportsCenter',
    shortdescription: 'Latest Sports News from ESPN',
    progtypetitle: 'Sports',
    genretitle: 'Sports',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportscenter',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283879171642&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 370,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 370,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8306',
    displayname: 'NBA Play-by-Play 216',
    shortdescription: 'Live NBA Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NBA Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nbaplaybyplay216',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795487&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 216,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 216,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9404',
    displayname: 'Carolina Shag Radio',
    shortdescription: 'Carolina R&B Beach Music',
    progtypetitle: 'Music',
    genretitle: 'Pop',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/carolinashagradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293953769636&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 701,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 701,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'la',
    displayname: 'Los Angeles',
    shortdescription: 'Los Angeles',
    progtypetitle: 'News & Issues',
    genretitle: 'Traffic/Weather',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/losangeles',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283876544320&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 136,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 136,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: 'newcountry',
    displayname: 'The Highway',
    shortdescription: 'Today\'s Country Hits',
    progtypetitle: 'Music',
    genretitle: 'Country',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/thehighway',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293947939961&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 56,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 56,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'hotjamz',
    displayname: 'The Heat',
    shortdescription: 'Today\'s R&B Hits',
    progtypetitle: 'Music',
    genretitle: 'Hip-Hop/R&B',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/theheat',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1407532026566&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 46,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 46,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9306',
    displayname: 'Edmonton Oilers',
    shortdescription: 'Oilers Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NHL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/edmontonoilers',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293952998683&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 931,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 931,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8322',
    displayname: 'NFL Play-by-Play 230',
    shortdescription: 'Home for every NFL game',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nflplaybyplay230',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293940121536&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 230,
        packages: {
          siriusxmallaccess: true
        }
      },
      online: {
        number: 230,
        packages: {
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: 'cnnespanol',
    displayname: 'CNN en Espanol',
    shortdescription: 'CNN\'s 24-Hour Spanish-language News',
    progtypetitle: 'News & Issues',
    genretitle: 'News/Public Radio',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/cnnenespanol',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293952995426&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 795,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 795,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9407',
    displayname: 'Tom Petty Radio',
    shortdescription: 'Music From Rock Icon Tom Petty',
    progtypetitle: 'Music',
    genretitle: 'Rock',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/tompettyradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293946040837&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 31,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 31,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9187',
    displayname: 'Latidos',
    shortdescription: 'Latin Love Songs',
    progtypetitle: 'Music',
    genretitle: 'Latino',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/latidos',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283879389337&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 764,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 764,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9138',
    displayname: 'SiriusXM Spotlight',
    shortdescription: 'Discover Amazing Channels',
    progtypetitle: 'Music',
    genretitle: 'Pop',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxmspotlight',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293953021901&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 4,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 4,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9292',
    displayname: 'Sacramento Kings',
    shortdescription: 'Kings Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NBA Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nbasac',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293939017675&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 905,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 905,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8318',
    displayname: 'NFL Play-by-Play 226',
    shortdescription: 'Home for every NFL game',
    progtypetitle: 'Sports',
    genretitle: 'NFL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nflplaybyplay226',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293940121536&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 226,
        packages: {
          siriusxmallaccess: true
        }
      },
      online: {
        number: 226,
        packages: {
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: 'thepulse',
    displayname: 'The Pulse',
    shortdescription: 'Adult Pop Hits',
    progtypetitle: 'Music',
    genretitle: 'Pop',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/thepulse',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1407534556050&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 15,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 15,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9333',
    displayname: 'Sports Play-by-Play 989',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay989',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 989,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 989,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9282',
    displayname: 'Milwaukee Bucks',
    shortdescription: 'Bucks Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NBA Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nbamil',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293938974673&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 896,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 896,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9145',
    displayname: 'Studio 54 Radio',
    shortdescription: '\'70s-2000s Dance Hits',
    progtypetitle: 'Music',
    genretitle: 'Dance/Electronic',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/studio54radio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283879038688&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 54,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 54,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9211',
    displayname: 'Sports Play-by-Play 974',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay974',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 974,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 974,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9281',
    displayname: 'Miami Heat',
    shortdescription: 'Heat Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NBA Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nbamia',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293938976145&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 895,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 895,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9435',
    displayname: 'Sports Play-by-Play 385',
    shortdescription: 'Sports play by play',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay385',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293948976451&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 385,
        packages: {
          siriusxmallaccess: true
        }
      },
      online: {
        number: 385,
        packages: {
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9245',
    displayname: 'Miami Marlins',
    shortdescription: 'Marlins Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/miamimarlinsonline',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283888347375&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 854,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 854,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9458',
    displayname: 'SiriusXM SEC Radio',
    shortdescription: '24/7 SEC Talk & Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'Sports',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxmsecradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293953003082&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 374,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 374,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9452',
    displayname: 'NBC Sports Radio',
    shortdescription: '24/7 Sports Talk and PFT Live',
    progtypetitle: 'Sports',
    genretitle: 'Sports',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nbcsportsradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293952999785&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 205,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 205,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9327',
    displayname: 'Sports Play-by-Play 983',
    shortdescription: 'College Sports PXP & More',
    progtypetitle: 'Sports',
    genretitle: 'Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/sportsplaybyplay983',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009796020&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 983,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 983,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9257',
    displayname: 'Tampa Bay Rays',
    shortdescription: 'Rays Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'MLB Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/tampabayraysonline',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283888378873&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 866,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 866,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '90salternative',
    displayname: 'Lithium',
    shortdescription: '\'90s Alternative/Grunge',
    progtypetitle: 'Music',
    genretitle: 'Rock',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/lithium',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795391&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 34,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 34,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9416',
    displayname: 'The Covers Channel',
    shortdescription: '24/7 Cover Songs',
    progtypetitle: 'Music',
    genretitle: 'Pop',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/thecoverschannel',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293946041952&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 302,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 302,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9353',
    displayname: 'SiriusXM Limited Edition 3',
    shortdescription: 'Home for limited-run channels',
    progtypetitle: 'Music',
    genretitle: 'Rock',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxmlimitededition3',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293944375289&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 717,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 717,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9315',
    displayname: 'Ottawa Senators',
    shortdescription: 'Senators Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NHL Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/ottawasenators',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283900813700&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 940,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 940,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '8304',
    displayname: 'NBA Play-by-Play 214',
    shortdescription: 'Live NBA Play-by-Play',
    progtypetitle: 'Sports',
    genretitle: 'NBA Play-by-Play',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/nbaplaybyplay214',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1282009795487&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 214,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 214,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      }
    }
  },
  {
    id: '9362',
    displayname: 'Elevations',
    shortdescription: 'Reimagined Pop & Rock Classics',
    progtypetitle: 'Music',
    genretitle: 'Pop',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/elevations',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1383895056486&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 706,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 706,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '60svibrations',
    displayname: '\'60s on 6',
    shortdescription: '\'60s Pop Hits w/ Cousin Brucie',
    progtypetitle: 'Music',
    genretitle: 'Pop',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/60son6',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1283872667282&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 6,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 6,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'siriusnascarradio',
    displayname: 'SiriusXM NASCAR Radio',
    shortdescription: '24/7 NASCAR Talk & Races',
    progtypetitle: 'Sports',
    genretitle: 'Sports',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusnascarradio',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293953000445&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 90,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 90,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9421',
    displayname: 'The Garth Channel',
    shortdescription: 'Garth\'s Own Channel, 24/7',
    progtypetitle: 'Music',
    genretitle: 'Country',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/thegarthchannel',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293948109927&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 55,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 55,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: '9405',
    displayname: 'SiriusXM Limited Edition11',
    shortdescription: 'Home for limited-run channels',
    progtypetitle: 'Talk & Entertainment',
    genretitle: 'Entertainment',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/siriusxmlimitededition11',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1293944379082&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 790,
        packages: {
          SiriusXMSelect: false,
          siriusxmallaccess: false
        }
      },
      online: {
        number: 790,
        packages: {
          SiriusXMSelect: true,
          siriusxmallaccess: true
        }
      }
    }
  },
  {
    id: 'totally70s',
    displayname: '\'70s on 7',
    shortdescription: '\'70s Pop Hits',
    progtypetitle: 'Music',
    genretitle: 'Pop',
    artistsyouhear: [],
    favorite: false,
    vanityurl: '/70son7',
    adultcontent: false,
    smalllogo: 'https://www.siriusxm.com/servlet/Satellite?blobcol=urlimage&blobheader=image%2Fgif&blobkey=id&blobtable=ImageAsset&blobwhere=1439177222701&ssbinary=true',
    siriusxm: {
      satellite: {
        number: 7,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      },
      online: {
        number: 7,
        packages: {
          SiriusXMSelect: true,
          SiriusXMMostlyMusic: true,
          siriusxmallaccess: true
        }
      }
    }
  }
];
