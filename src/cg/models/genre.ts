export class Genre {
  name: string;
  key: string;
  category: string;
}
