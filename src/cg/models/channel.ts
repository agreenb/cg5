export class Channel {
  id: any;
  displayname: string;
  shortdescription: string;
  progtypetitle: string;
  genretitle: string;
  artistsyouhear?: string[];
  vanityurl: string;
  adultcontent: boolean;
  favorite: boolean;
  smalllogo: string;
  siriusxm?: {
    satellite?: {
      number?: number;
      packages?: {
        SiriusXMSelect?: boolean;
        SiriusXMMostlyMusic?: boolean;
        siriusxmallaccess?: boolean;
      }
    };
    online?: {
      number?: number;
      packages?: {
        SiriusXMSelect?: boolean;
        SiriusXMMostlyMusic?: boolean;
        siriusxmallaccess?: boolean;
      }
    }
  };
}
